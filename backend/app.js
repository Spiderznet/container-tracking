const express = require('express');
const path = require('path');
const logger = require('morgan');
const bindResponse = require('./utils/bindResponse');
const cors = require('cors')

const app = express();

app.use(cors())
app.use(bindResponse)
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public'), { dotfiles: 'allow' }));
app.use(express.static(path.join(__dirname, '../admin/build'), { dotfiles: 'allow' }));
app.use('/application', express.static(path.join(__dirname, '../application/build'), { dotfiles: 'allow' }));
app.use((req, res, next) => {
    let { body = {}, query, params } = req
    if (query.qs) {
        query = {
            ...query,
            ...JSON.parse(query.qs)
        }
        delete query.qs
    }
    req.state = { body, query, params, msg: '', result: '' }
    next()
})

app.use('/api', require('./routes'));

app.use((req, res, next) => {
    let { msg = "", result } = req.state
    msg = msg || "Request Success"
    res.pass(msg, result)
})

app.use((err, req, res, next) => {
    let { msg = "" } = req.state
    msg = msg || err || "Request Faild"
    res.fail(msg, err.toString())
})

module.exports = app;
