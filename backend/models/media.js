const multer = require('multer')
const fs = require('fs')
const Jimp = require('jimp');
const { USER_ROLE } = require('../config');



var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        let dirName = 'public/uploads'
        if (!fs.existsSync(dirName)) {
            fs.mkdirSync(dirName, { recursive: true })
        }
        callback(null, dirName)
    },
    filename: function (req, file, callback) {
        callback(null, `${Date.now().toString(36)}-${file.originalname}`)
    }
});
var upload = multer({ storage }).single('file')


module.exports = {
    title: 'Media',
    path: '/media',
    name: 'media',
    icon: 'iconsminds-box-with-folders',
    roles: [USER_ROLE.Admin, USER_ROLE.root],
    beforeCreate: [upload, async (req, res, next) => {
        if (req.file) {
            let { file } = req
            await Jimp.read(file.path).then(image => {
                let [name] = file.filename.split('.')
                let thumbImage = file.path.replace(name, 'thumbnail-' + name)
                return image
                    .resize(480, Jimp.AUTO)
                    .quality(80)
                    .write(thumbImage)
            })
            let body = {
                name: file.filename,
                path: file.destination.replace('public', '') + '/' + file.filename,
                size: file.size,
                type: file.mimetype,
                attributes: {
                    originalname: file.originalname
                },
                userId: req.state.user.userId
            }
            req.state.body = {
                ...req.state.body,
                ...body
            }
        }
        next()
    }],
    afterDelete: async (req, res, next) => {
        let { result } = req.state
        if (result && result.path) {
            let path = 'public/' + result.path
            if (fs.existsSync(path)) {
                fs.unlinkSync(path)
                fs.unlinkSync(path.replace(result.name, 'thumbnail-' + result.name))
            }
        }
        next()
    },
    fields: [
        {
            name: "id",
            type: "Int",
            kind: 'scalar',
            create: false
        },
        {
            name: "name",
            type: "String",
            kind: 'scalar',
            create: {
                rule: 'required'
            }
        },
        {
            name: "path",
            type: "String",
            kind: 'scalar',
            create: {
                rule: 'required'
            }
        },
        {
            name: "size",
            type: "Int",
            kind: 'scalar',
            create: {
                rule: 'required'
            }
        },
        {
            name: "type",
            type: "String",
            kind: 'scalar',
            create: {
                rule: 'required'
            }
        },
        {
            name: "attributes",
            type: "String",
            kind: 'scalar',
            create: true
        },
        {
            name: "userId",
            type: "Int",
            kind: 'scalar',
            create: {
                rule: 'required'
            }
        },
        {
            name: "createdAt",
            type: "DateTime",
            kind: 'scalar',
            create: false
        },
        {
            name: "updatedAt",
            type: "DateTime",
            kind: 'scalar',
            create: false
        },
        {
            name: "user",
            type: "User",
            kind: 'object',
            create: false
        },
        {
            name: "entry",
            type: "Entry",
            kind: 'object',
            browse: false,
            create: false
        }
    ]
}