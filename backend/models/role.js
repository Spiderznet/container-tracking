const { USER_ROLE } = require("../config");


module.exports = {
    title: 'Role',
    path: '/roles',
    name: 'role',
    icon: 'simple-icon-user-following',
    roles: [USER_ROLE.root],
    fields: [
        {
            name: "id",
            type: "Int",
            kind: "scalar",
            create: false,
        },
        {
            name: "name",
            type: "String",
            kind: "scalar",
            create: {
                rule: 'required'
            }
        },
        {
            name: "description",
            type: "String",
            kind: "scalar",
            create: {
                rule: 'required'
            }
        },
        {
            name: "createdAt",
            type: "DateTime",
            kind: "scalar",
            create: false,
        },
        {
            name: "updatedAt",
            type: "DateTime",
            kind: "scalar",
            create: false,
        },
        {
            name: "user",
            type: "User",
            kind: "object",
            browse: false,
            create: false,
        }
    ]
}