const { USER_ROLE } = require("../config");


module.exports = {
    title: 'Container',
    path: '/containers',
    name: 'container',
    icon: 'iconsminds-ship',
    roles: [USER_ROLE.Admin, USER_ROLE.root],
    fields: [
        {
            name: "id",
            type: "String",
            kind: "scalar",
            create: {
                rule: 'required|min:11|max:11'
            }
        },
        {
            name: "createdAt",
            type: "DateTime",
            kind: "scalar",
            create: false,
        },
        {
            name: "updatedAt",
            type: "DateTime",
            kind: "scalar",
            create: false,
        },
        {
            name: "entry",
            type: "Entry",
            kind: "object",
            browse: false,
            create: false,
        }
    ]
}