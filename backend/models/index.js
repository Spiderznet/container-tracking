const fs = require('fs')

const models = []

let files = fs.readdirSync(__dirname)
    .filter(f => f.endsWith('.js') && !['index.js', 'enum.js'].includes(f))
    .map(i => i.replace('.js', ''));


for (const file of files) {
    models.push(require(`./${file}`))
}

module.exports = models