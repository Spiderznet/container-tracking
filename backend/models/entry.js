const { USER_ROLE } = require("../config");


module.exports = {
    title: 'Entry',
    path: '/entries',
    name: 'entry',
    icon: 'iconsminds-notepad',
    roles: [USER_ROLE.Admin, USER_ROLE.root],
    beforeGetAll: (req, res, next) => {
        let { user, query } = req.state
        if (![USER_ROLE.Admin, USER_ROLE.root].includes(user.roleId)) {
            req.state.query._where = {
                ...query._where,
                userId: user.userId
            }
        }
        next()
    },
    config: {
        include: {
            user: true,
            terminal: true,
            shift: true,
            container: true,
            image: true
        }
    },
    fields: [
        {
            name: "id",
            type: "Int",
            kind: "scalar",
            create: false,
        },
        {
            name: "userId",
            type: "Int",
            kind: "scalar",
            browse: false,
            create: false,
        },
        {
            name: "user",
            type: "User",
            kind: "object",
            relationFrom: 'userId',
            relationTo: 'id',
            browse: {
                field: 'name'
            },
            create: {
                field: 'name',
                rule: 'required'
            },
        },
        {
            name: "shiftId",
            type: "Int",
            kind: "scalar",
            browse: false,
            create: false,
        },
        {
            name: "shift",
            type: "Shift",
            kind: "object",
            relationFrom: 'shiftId',
            relationTo: 'id',
            browse: {
                field: 'name'
            },
            create: {
                field: 'name',
                rule: 'required'
            },
        },
        {
            name: "terminalId",
            type: "Int",
            kind: "scalar",
            browse: false,
            create: false,
        },
        {
            name: "terminal",
            type: "Terminal",
            kind: "object",
            relationFrom: 'terminalId',
            relationTo: 'id',
            browse: {
                field: 'name'
            },
            create: {
                field: 'name',
                rule: 'required'

            },
        },
        {
            name: "containerId",
            type: "String",
            kind: "scalar",
            browse: false,
            create: false,
        },
        {
            name: "container",
            type: "Container",
            kind: "object",
            relationFrom: 'containerId',
            relationTo: 'id',
            browse: {
                field: 'id'
            },
            create: {
                field: 'id',
                rule: 'required'
            },
        },
        {
            name: "imageId",
            type: "Int",
            kind: "scalar",
            browse: false,
            create: false,
        },
        {
            name: "image",
            type: "Media",
            kind: "object",
            relationFrom: 'imageId',
            relationTo: 'id',
            browse: {
                field: 'name',
            },
            create: {
                rule: 'required',
                field: 'name',
            },
        },
        {
            name: "createdAt",
            type: "DateTime",
            kind: "scalar",
            create: false,
        },
        {
            name: "updatedAt",
            type: "DateTime",
            kind: "scalar",
            create: false,
        }
    ]
}