const { USER_ROLE } = require("../config");


module.exports = {
    title: 'Shift',
    path: '/shifts',
    name: 'shift',
    icon: 'iconsminds-timer',
    roles: [USER_ROLE.Admin, USER_ROLE.root],
    fields: [
        {
            name: "id",
            type: "Int",
            kind: "scalar",
            create: false
        },
        {
            name: "name",
            type: "String",
            kind: "scalar",
            create: {
                rule: 'required'
            }
        },
        {
            name: "startTime",
            type: "String",
            kind: "scalar",
            create: {
                rule: 'required'
            }
        },
        {
            name: "endTime",
            type: "String",
            kind: "scalar",
            create: {
                rule: 'required'
            }
        },
        {
            name: "createdAt",
            type: "DateTime",
            kind: "scalar",
            create: false
        },
        {
            name: "updatedAt",
            type: "DateTime",
            kind: "scalar",
            create: false
        },
        {
            name: "entry",
            type: "Entry",
            kind: "object",
            browse: false,
            create: false
        }
    ]
}