const { USER_ROLE } = require("../config");


module.exports = {
    title: 'Terminal',
    path: '/terminals',
    name: 'terminal',
    icon: 'simple-icon-location-pin',
    roles: [USER_ROLE.Admin, USER_ROLE.root],
    fields: [
        {
            name: "id",
            type: "Int",
            kind: 'scalar',
            create: false
        },
        {
            name: "name",
            type: "String",
            kind: 'scalar',
            create: {
                rule: 'required'
            }
        },
        {
            name: "location",
            type: "String",
            kind: 'scalar',
            create: {
                rule: 'required'
            }
        },
        {
            name: "createdAt",
            type: "DateTime",
            kind: 'scalar',
            create: false
        },
        {
            name: "updatedAt",
            type: "DateTime",
            kind: 'scalar',
            create: false
        },
        {
            name: "entry",
            type: "Entry",
            kind: 'object',
            browse: false,
            create: false
        }
    ]
}