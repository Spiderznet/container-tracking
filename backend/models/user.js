const { USER_ROLE } = require("../config")
var { SHA256: hash } = require('crypto-js');


module.exports = {
    title: 'User',
    path: '/users',
    name: 'user',
    icon: 'simple-icon-people',
    roles: [USER_ROLE.Admin, USER_ROLE.root],
    config: {
        include: {
            role: true,
            image: true,
            // Entry: true,
        }
    },
    beforeRequest: (req, res, next) => {
        console.log('beforeRequest')
        next()
    },
    beforeResponse: (req, res, next) => {
        console.log('beforeResponse')
        let { result } = req.state
        if (Array.isArray(result)) {
            result = result.map(item => {
                if (item.password) {
                    delete item.password
                }
                return item
            })
        }
        else if (result.password) {
            delete result.password
        }
        req.state.result = result
        next()
    },
    beforeCreate: (req, res, next) => {
        console.log('beforeCreate')
        let { body } = req.state
        if (body.password) {
            req.state.body.password = hash(body.password).toString()
        }
        next()
    },
    fields: [
        {
            name: "id",
            type: "Int",
            kind: "scalar",
            browse: true,
            create: false,
        },
        {
            name: "name",
            type: "String",
            browse: true,
            kind: "scalar",
            create: {
                rule: 'required'
            }
        },
        {
            name: "phone",
            type: "String",
            kind: "scalar",
            browse: true,
            create: {
                rule: 'required'
            }
        },
        {
            name: "password",
            type: "String",
            kind: "scalar",
            browse: false,
            create: {
                rule: 'required'
            }
        },
        {
            name: "roleId",
            type: "Int",
            kind: "scalar",
            browse: false,
            create: false
        },
        {
            name: "role",
            type: "Role",
            kind: "object",
            relationFrom: 'roleId',
            relationTo: 'id',
            browse: {
                field: 'name'
            },
            create: {
                rule: 'required',
                field: 'name'
            }
        },
        {
            name: "imageId",
            type: "Int",
            kind: "scalar",
            browse: false,
            create: false
        },
        {
            name: "image",
            type: "Media",
            kind: "object",
            relationFrom: 'imageId',
            relationTo: 'id',
            browse: {
                field: 'name'
            },
            create: {
                rule: 'required',
                field: 'name'
            }
        },
        {
            name: "isActive",
            type: "Status",
            kind: "enum",
            create: {
                rule: 'required'
            },
        },
        {
            name: "createdAt",
            type: "DateTime",
            kind: "scalar",
            create: false,
        },
        {
            name: "updatedAt",
            type: "DateTime",
            kind: "scalar",
            create: false,
        },
        {
            name: "entry",
            type: "Entry",
            browse: false,
            create: false,
        }
    ],

}