

const USER_ROLE = {
    root: 1,
    Admin: 2,
    user: 3,
    driver: 4,
}

module.exports = {
    USER_ROLE
}