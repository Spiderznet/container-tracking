let jwt = require('jsonwebtoken');

let SHA = '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'

let createJWToken = (data = "") => {
    // let iat = new Date().getTime();
    // let hours = 24 * 360;
    // let exp = iat + (1000 * 60 * 60) * hours;
    return jwt.sign(data, SHA, {
        //  expiresIn: '30s'
    });
}

let getUserIfExist = (req, res, next) => {
    let { 'authorization': token = "" } = req.headers || {};
    token = token || req.body.token;
    token = token.replace('Bearer ', '')
    if (token) {
        jwt.verify(token, SHA, (err, decode) => {

            if (err) return next(err)

            req.state.user = decode
            next()
        });
    }
    else {
        next()
    }
}


let verifyJWTToken = (req, res, next) => {
    let { 'authorization': token = "" } = req.headers || {};
    token = token || req.body.token || '';
    token = token.replace('Bearer ', '')
    if (token) {
        jwt.verify(token, SHA, (err, decode) => {

            if (err) return next('Token expired');

            req.state.user = decode
            next()
        });
    }
    else {
        next('Token missing')
    }
}

const getInfo = (token, callback) => {
    if (token) {
        jwt.verify(token, SHA, (err, decode) => {

            if (err) return callback(err);

            if ('exp' in decode && decode.exp - new Date().getTime() > 0) {
                callback(null, decode);
            }
            else {
                callback('Token expired')
            }
        });
    }
    else {
        callback('Token missing')
    }
}

module.exports = {
    createJWToken,
    verifyJWTToken,
    getUserIfExist,
    getInfo
}