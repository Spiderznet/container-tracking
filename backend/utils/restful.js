
const defaultMiddleware = (req, res, next) => {
    next()
}

const restfulServices = ({
    name,
    title,
    beforeRequest = defaultMiddleware,
    beforeResponse = defaultMiddleware,
    beforeGetAll = defaultMiddleware,
    afterGetAll = defaultMiddleware,
    beforeGetDetails = defaultMiddleware,
    aftereGetDetails = defaultMiddleware,
    beforeCreate = defaultMiddleware,
    afterCreate = defaultMiddleware,
    beforeUpdate = defaultMiddleware,
    afterUpdate = defaultMiddleware,
    beforeDelete = defaultMiddleware,
    afterDelete = defaultMiddleware,
    config = {},
    fields,
}) => {

    const express = require('express');
    const { PrismaClient } = require('@prisma/client')
    const { verifyJWTToken } = require('../utils/jwt')
    const Validator = require('validatorjs');
    const { headerCase } = require('change-case')
    const router = express.Router();
    const prisma = new PrismaClient()
    const { type = "String" } = fields.find(f => f.name === 'id') || {}
    const idIsNumber = type !== "String"

    const Modal = prisma[name]
    router.use(verifyJWTToken)
    router.use(beforeRequest)

    router.get('/', beforeGetAll, async (req, res, next) => {
        try {
            let { user, query } = req.state
            let { _where = {}, _sort = {}, _limit, _offset } = query
            let result = await Modal.findMany({
                where: {
                    ..._where
                },
                orderBy: {
                    ..._sort
                },
                take: _limit,
                skip: _offset,
                ...config
            })
            req.state.msg = `${title} List successfuly`
            req.state.result = result
            next()
        }
        catch (err) {
            next(err)
        }
    }, afterGetAll)
    router.get('/count', async (req, res, next) => {
        try {
            let result = await Modal.count()
            res.send({
                status: 1,
                msg: `${title} count request success`,
                data: result
            })
        }
        catch (err) {
            next(err)
        }
    })
    router.get('/:id', beforeGetDetails, async (req, res, next) => {
        try {
            let { id } = req.params
            let result = await Modal.findUnique({
                where: { id: idIsNumber ? Number(id) : id },
                ...config
            })
            req.state.msg = `${title} Details successfuly`
            req.state.result = result
            next()
        }
        catch (err) {
            next(err)
        }
    }, aftereGetDetails)

    router.post('/', beforeCreate, async (req, res, next) => {
        try {
            let { body } = req.state
            let rules = fields.reduce((final, field) => {
                let { create, name, relationFrom, type, kind } = field
                if (create && create.rule) {
                    if (kind === 'object') {
                        final[name] = {
                            [create.field]: create.rule
                        }
                    }
                    if (kind === 'enum') {
                        final[name] = {
                            ['id']: create.rule
                        }
                    }
                    else {
                        final[name] = create.rule
                    }
                }
                return final
            }, {})
            let validation = new Validator(body, rules)
            if (validation.fails()) {
                res.fail(
                    'Validation error',
                    Object.keys(validation.errors.errors).reduce((final, errorName) => {
                        let [name] = errorName.split('.')
                        final[name] = validation.errors.errors[errorName].join(', ').replace(errorName, headerCase(name))
                        return final
                    }, {})
                )
                return
            }
            let data = fields.reduce((final, field) => {
                let { create = true, name, type, relationFrom, relationTo, kind } = field
                if (!Object.keys(body).includes(name)) return final

                if (create) {
                    if (kind === 'scalar') {
                        final[name] = body[name]
                    }
                    else if (kind === 'enum') {
                        final[name] = body[name]['id']
                    }
                    else {
                        final[relationFrom] = body[name]?.[relationTo]
                    }
                }
                return final
            }, {})
            let result = await Modal.create({
                data,
                ...config
            })
            req.state.msg = `${title} Create successfuly`
            req.state.result = result
            next()
        }
        catch (err) {
            next(err)
        }
    }, afterCreate)

    router.put('/:id', beforeUpdate, async (req, res, next) => {
        try {
            let { body } = req.state
            let { id } = req.params

            let rules = fields.reduce((final, field) => {
                let { create, name, relationFrom, type, kind } = field
                if (create && create.rule) {
                    if (kind === 'object') {
                        final[name] = {
                            [create.field]: create.rule
                        }
                    }
                    if (kind === 'enum') {
                        final[name] = {
                            ['id']: create.rule
                        }
                    }
                    else {
                        final[name] = create.rule
                    }
                }
                return final
            }, {})
            let validation = new Validator(body, rules)
            if (validation.fails()) {
                res.fail(
                    'Validation error',
                    Object.keys(validation.errors.errors).reduce((final, errorName) => {
                        let [name] = errorName.split('.')
                        final[name] = validation.errors.errors[errorName].join(', ').replace(errorName, headerCase(name))
                        return final
                    }, {})
                )
                return
            }
            let data = fields.reduce((final, field) => {
                let { create = true, name, type, relationFrom, relationTo, kind } = field
                if (!Object.keys(body).includes(name)) return final

                if (create) {
                    if (kind === 'scalar') {
                        final[name] = body[name]
                    }
                    else if (kind === 'enum') {
                        final[name] = body[name]['id']
                    }
                    else {
                        final[relationFrom] = body[name]?.[relationTo]
                    }
                }
                return final
            }, {})
            let result = await Modal.update({
                where: { id: idIsNumber ? Number(id) : id },
                data,
                ...config
            })


            req.state.msg = `${title} update successfuly`
            req.state.result = result
            next()
        }
        catch (err) {
            next(err)
        }
    }, afterUpdate)

    router.delete('/:id', beforeDelete, async (req, res, next) => {
        try {
            let { id } = req.params
            let result = await Modal.delete({
                where: { id: idIsNumber ? Number(id) : id },
                ...config
            })
            req.state.msg = `${title} Delete successfuly`
            req.state.result = result
            next()
        }
        catch (err) {
            next(err)
        }
    }, afterDelete)

    router.use(beforeResponse)

    return router
}

module.exports = restfulServices