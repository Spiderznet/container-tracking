let bindResponse = (req, res, next) => {
    var _send = res.send;
    var sent = false;
    res.pass = function (msg = "success", data = {}) {
        if (sent) return;
        _send.bind(res)({
            status: 1,
            msg,
            data
        });
        sent = true;
    };
    res.fail = function (msg = "", data = {}) {
        if (sent) return;
        _send.bind(res)({
            status: 0,
            msg,
            data
        });
        sent = true;
    };
    next();
}
module.exports = bindResponse;