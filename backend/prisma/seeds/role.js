module.exports = [
    {
        name: 'root',
        description: 'root',
    },
    {
        name: 'admin',
        description: 'Admin',
    },
    {
        name: 'user',
        description: 'Normal User',
    },
    {
        name: 'driver',
        description: 'Vehicle Driver',
    },
]