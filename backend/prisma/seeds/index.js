const fs = require('fs')

let seedFiles = fs.readdirSync(__dirname)

seedFiles = seedFiles
    .filter(f => f.endsWith('.js'))
    .map(i => i.replace('.js', ''))
    .filter(f => !['index'].includes(f))

const seeds = {}

for (const seedName of seedFiles) {
    let seedData = require(`./${seedName}`)

    seeds[seedName] = seedData
}

module.exports = seeds