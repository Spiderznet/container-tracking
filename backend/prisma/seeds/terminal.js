module.exports = [
    { name: 'CP1', location: 'Chennai Port' },
    { name: 'CP2', location: 'Visakhapatnam Port Trust' },
    { name: 'CP3', location: 'Kochi Port Trust' },
    { name: 'CP4', location: 'Kamarajar Port Limited' }
]