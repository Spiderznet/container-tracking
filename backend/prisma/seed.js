var { PrismaClient } = require('@prisma/client')
var seeds = require('./seeds')
var prisma = new PrismaClient()

const main = async () => {
    try {
        let models = Object.keys(prisma).filter(f => !f.startsWith('_'))
        for (const seedName of models) {
            if (Object.keys(seeds).includes(seedName)) {
                for (const inx in seeds[seedName]) {
                    await prisma[seedName].create({
                        data: seeds[seedName][inx]
                    })
                }
                console.log(`${seedName} seed complete!`)
            }
        }
    }
    catch (err) {
        throw err
    }
}

main().catch(e => {
    throw e
}).finally(async () => {
    await prisma.$disconnect()
})