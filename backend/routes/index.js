const express = require('express');
const models = require('../models/index');
const enumTypes = require('../models/enum');
const restfulServices = require('../utils/restful');
const fs = require('fs')
const router = express.Router();
let routes = fs.readdirSync(__dirname)


for (const model of models) {
    let { path } = model
    console.log(`${path} path used ${model.title} model`)
    router.use(path, restfulServices(model))
}


routes = routes
    .filter(f => f.endsWith('.js'))
    .filter(f => !['index.js'].includes(f))
    .map(i => i.replace('.js', ''))

for (const item of routes) {
    console.log(`/${item} path used ${item} route`)
    router.use(`/${item}`, require(`./${item}`))
}

/* GET home page. */
router.get('/', function (req, res, next) {
    res.pass('Container Tracking API')
});

router.get('/app-config', function (req, res, next) {

    req.state.result = {
        enums: enumTypes,
        models
    }
    next()
});

module.exports = router;
