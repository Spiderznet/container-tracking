const express = require('express');
const { PrismaClient } = require('@prisma/client');
var { SHA256: hash } = require('crypto-js');
const { createJWToken, verifyJWTToken } = require('../utils/jwt');
const { USER_ROLE } = require('../config');
const router = express.Router();
const prisma = new PrismaClient()

router.get('/', (req, res) => {
    // return res.send(Object.keys(prisma._dmmf))
    return res.send(prisma._dmmf.datamodel)
})

router.get('/drivers/:phone', async (req, res, next) => {
    try {
        let { phone } = req.params
        let result = await prisma.user.findMany({
            where: {
                phone,
                roleId: USER_ROLE.driver
            },
            select: {
                id: true,
                phone: true,
                name: true,
                roleId: true
            }
        })
        result = result.map(item => {
            let tokenBody = { userId: item.id, name: item.name, roleId: item.roleId }
            let token = createJWToken(tokenBody);
            item.token = token
            return item
        })
        req.state.msg = 'Drivers list'
        req.state.result = result
        next()
    }
    catch (err) {
        next(err)
    }
})

router.post('/login', async (req, res, next) => {
    try {
        let { body } = req.state
        let result = await prisma.user.findFirst({
            where: {
                phone: body.username,
                password: hash(body.password).toString()
            }
        })
        console.log(result)
        if (result) {
            if (hash(body.password).toString() == result.password) {
                let tokenBody = { userId: result.id, name: result.name, roleId: result.roleId }
                let token = createJWToken(tokenBody);

                req.state.msg = 'Login successfully'
                req.state.result = { ...tokenBody, token }
                next()
            }
            else {
                next('Password dos\'t match')
            }
        }
        else {
            next('Invalid username and Password')
        }
    } catch (err) {
        next(err)

    }
});

router.get('/check', verifyJWTToken, (req, res, next) => {
    res.send(req.state)
})
module.exports = router;
