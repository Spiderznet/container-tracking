const express = require('express');
const { PrismaClient } = require('@prisma/client');
const multer = require('multer')
const fs = require('fs')
const vision = require('@google-cloud/vision');
const Jimp = require('jimp')


const router = express.Router();
const prisma = new PrismaClient()

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        let dirName = 'public/uploads'
        if (!fs.existsSync(dirName)) {
            fs.mkdirSync(dirName, { recursive: true })
        }
        callback(null, dirName)
    },
    filename: function (req, file, callback) {
        callback(null, `${Date.now().toString(36)}-${file.originalname}`)
    }
});
var upload = multer({ storage })



router.post('/read-image', upload.single('file'), async (req, res, next) => {
    try {
        let { file } = req
        let { terminalId, shiftId } = req.body
        // Creates a client
        const client = new vision.ImageAnnotatorClient({
            keyFile: 'vision-key.json'
        });

        await Jimp.read(file.path).then(image => {
            let [name] = file.filename.split('.')
            let thumbImage = file.path.replace(name, 'thumbnail-' + name)
            return image
                .resize(480, Jimp.AUTO)
                .quality(80)
                .write(thumbImage)
        })

        let mediaResult = await prisma.media.create({
            data: {
                name: file.filename,
                type: file.mimetype,
                size: file.size,
                path: file.destination.replace('public', '') + '/' + file.filename,
                attributes: {
                    originalname: file.originalname
                },
                userId: req.state.user.userId
            }
        })

        req.state.result = {
            image: mediaResult,
            containerId: 'processing'
        }
        next()
        // Performs label detection on the image file
        const [result] = await client.documentTextDetection(req.file.path);
        let fullText = result.fullTextAnnotation.text
        let [firstRow = ''] = fullText.match(/[A-Z0]{3}U/gi) || []
        let [secondRow = ''] = fullText.match(/[0-9O]{6} [0-9O]|[0-9O]{6}[0-9O]/gi) || []
        firstRow = firstRow || '';
        secondRow = secondRow || '';
        firstRow = firstRow.replace(/0/g, 'O')
        secondRow = secondRow.replace(/O/g, '0').replace(/ /g, '')
        let containerNumber = firstRow + secondRow
        let containerResult = null
        if (containerNumber.length === 11) {
            containerResult = await prisma.container.upsert({
                where: {
                    id: containerNumber
                },
                create: {
                    id: containerNumber
                },
                update: {}
            })
        }

        let entryResult = await prisma.entry.create({
            data: {
                containerId: containerNumber.length === 11 ? containerResult.id : null,
                imageId: mediaResult.id,
                shiftId: Number(shiftId),
                terminalId: Number(terminalId),
                userId: req.state.user.userId
            }
        })

    }
    catch (err) {
        console.log(err)
        next(err)
    }
})


module.exports = router