export const API_ROOT = process.env.REACT_APP_API_ROOT
export const API_BASE = process.env.REACT_APP_API_BASE
export const PUBLIC_URL = process.env.PUBLIC_URL;