

export const SCREENS = {
    HOME: '/',
    ONBOARD: '/onboard',
    TERMINALS: '/terminals',
    SHIFTS: '/shifts',
    ENTRIES: '/entries',
    NEW_ENTRY: '/new-entry',
}

export const LOCAL_STORAGE = {
    TOKEN:'token',
    TERMINAL:'terminal',
    SHIFT:'shift',
    STORE:'store',
    USER_ID:'user_id',
}