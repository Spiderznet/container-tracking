import Axios from 'axios';
import { API_ROOT } from '../config';
import Swal from 'sweetalert2'
import { LOCAL_STORAGE } from './constants';

const getOptions = () => {
    let options = {};
    let token = localStorage.getItem(LOCAL_STORAGE.TOKEN);
    if (token) {
        options = {
            ...options,
            headers: {
                'Authorization': `Bearer ${token}`
            }

        }
    }

    return options
}

const parseResponse = res => res.data;

const errorInfo = (res, err) => {
    if (!res.status) {
        Swal.fire('Sorry!', res.msg, 'error')
        throw new Error(res.msg)
    }
    if (err) {
        Swal.fire('Sorry!', err.toString(), 'error')
        // throw err
    }
    return res
}

export const http = {
    get: (url, config = {}) => Axios.get(`${API_ROOT}${url}`, { ...getOptions(), ...config }).then(parseResponse).then(errorInfo),
    post: (url, data, config = {}) => Axios.post(`${API_ROOT}${url}`, data, { ...getOptions(), ...config }).then(parseResponse).then(errorInfo),
    put: (url, data, config = {}) => Axios.put(`${API_ROOT}${url}`, data, { ...getOptions(), ...config }).then(parseResponse).then(errorInfo),
    delete: (url, config = {}) => Axios.delete(`${API_ROOT}${url}`, { ...getOptions(), ...config }).then(parseResponse).then(errorInfo),
}

export const AuthServices = (path => ({
    login: (data) => http.post(`${path}/login`, data),
    getDrivers: (phone) => http.get(`${path}/drivers/${phone}`),
}))('/auth')

export const UserServices = ((path, config) => ({
    all: () => http.post(`${path}/login`, config)
}))('/users')

export const TerminalServices = (path => ({
    all: () => http.get(`${path}`)
}))('/terminals')

export const ShiftsServices = (path => ({
    all: () => http.get(`${path}`)
}))('/shifts')

export const EntriesServices = (path => ({
    all: (config) => http.get(`${path}`, config),
    create: (data) => http.post(`${path}`, data),
    getContainerDetails: (data, config) => http.post(`${path}/read-image`, data, config)
}))('/entries')