import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from "react-router-dom";
import "./assets/scss/app.scss";
import { Provider } from 'react-redux'
import store from './redux/store';
import { PUBLIC_URL } from './config';

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider >,
  document.getElementById('root')
);


if (process.env.NODE_ENV === "production") {

  if ('serviceWorker' in navigator) {
      // attach event listener  on page l aod
      window.addEventListener('load', () => {

          // register serviceWorker withthe [sw.js] file
          navigator.serviceWorker.register(`${PUBLIC_URL}/sw.js`).then(registration => {

              console.log('ServiceWorker registration successful with scope: ', registration.scope);

          }, function (err) {
              // registration failed
              console.log('ServiceWorker registration failed: ', err);
          });

          navigator.serviceWorker.addEventListener('controllerchange', () => {
              window.location.reload()
          })

      });
  }
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
