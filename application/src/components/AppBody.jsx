

const AppBody = ({ children, className, ...props }) => {
    return (
        <div className={["app-body", className].join(' ')} style={{ backgroundImage: `url(./assets/images/shape.svg)` }} {...props}>
            {children}
        </div>
    )
}

export default AppBody