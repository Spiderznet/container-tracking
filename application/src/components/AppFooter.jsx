

const AppFooter = ({ children, className, ...props }) => {
    return (
        <div className={["app-footer", className].join(' ')} {...props}>
            {children}
        </div>
    )
}

export default AppFooter