import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap'

const handleLogout = () => {
    localStorage.clear()
    window.location.href = '/'
}

const AppHeader = ({ children, className = "", ...props }) => {
    return (
        <div className={["app-title-bar", className].join(' ')} {...props}>
            {children}
        </div>
    )
}

export const Left = ({ children = "", className = "", ...props }) => {
    return (
        <div className={["left", className].join(' ')} {...props}>
            {children}
        </div>
    )
}

export const Body = ({ children = "", className = "", ...props }) => {
    return (
        <div className={["body", className].join(' ')} {...props}>
            {children}
        </div>
    )
}
export const Right = ({ children = "", className = "", ...props }) => {
    return (
        <div className={["right", className].join(' ')} {...props}>
            <UncontrolledDropdown >
                <DropdownToggle tag="a" className="nav-link text-white">
                    <i className="fas fa-ellipsis-v fa-lg"></i>
                </DropdownToggle>
                <DropdownMenu right className="p-0 border-0 rounded-0">
                    {children}
                    <DropdownItem tag="div" className="py-3" onClick={handleLogout} >Logout</DropdownItem>
                </DropdownMenu>
            </UncontrolledDropdown>
        </div>
    )
}

export const Title = ({ children = "", className = "", ...props }) => {
    return (
        <div className={["app-title", className].join(' ')}>{children}</div>
    )
}

export default AppHeader