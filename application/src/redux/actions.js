export const UPDATE_VALUE = 'UPDATE_VALUE'

export const updateValue = (payload = {}) => ({
    type: UPDATE_VALUE,
    payload
})