import { createStore, compose } from 'redux';
import { LOCAL_STORAGE } from '../common/constants';
import reducer from './reducer';


const devTools = 'production' !== process.env.NODE_ENV
    && typeof window === 'object'
    && window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f;

const middlewares = [devTools];

const store = createStore(
    reducer,
    compose(...middlewares)
);

store.subscribe(() => {
    let state = store.getState()
    localStorage.setItem(LOCAL_STORAGE.STORE, JSON.stringify(state))
})

if (module.hot) {
    module.hot.accept('./reducer', () => {
        const nextRootReducer = require('./reducer');
        store.replaceReducer(nextRootReducer);
    });
}
export default store