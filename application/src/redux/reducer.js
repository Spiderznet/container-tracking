import { LOCAL_STORAGE } from "../common/constants";
import { UPDATE_VALUE } from "./actions";

let storeData = localStorage.getItem(LOCAL_STORAGE.STORE)

const initState = storeData ? JSON.parse(storeData) : {
    terminal: null,
    shift: null
}

const reducer = (state = initState, { type, payload }) => {
    switch (type) {
        case UPDATE_VALUE:
            return {
                ...state,
                ...payload
            }

        default:
            return state
    }
}

export default reducer