import { Switch, Route } from 'react-router-dom'
import { SCREENS } from './common/constants'
import EntriesView from './views/EntriesView'
import NewEntryView from './views/NewEntryView'
import OnboardView from './views/OnboardView'
import ShiftsView from './views/ShiftsView'
import TerminalsView from './views/TerminalsView'
import WelcomeView from './views/WelcomeView'

const App = () => {
    return (
        <>
            <Switch>
                <Route path={SCREENS.HOME} component={WelcomeView} exact />
                <Route path={SCREENS.ONBOARD} component={OnboardView} exact />
                <Route path={SCREENS.TERMINALS} component={TerminalsView} exact />
                <Route path={SCREENS.SHIFTS} component={ShiftsView} exact />
                <Route path={SCREENS.ENTRIES} component={EntriesView} exact />
                <Route path={SCREENS.NEW_ENTRY} component={NewEntryView} exact />
            </Switch>
        </>
    )
}

export default App