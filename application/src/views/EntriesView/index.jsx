import AppBody from "../../components/AppBody"
import { useEffect, useRef, useState } from "react"
import { connect } from "react-redux"
import { updateValue } from "../../redux/actions"
import { SCREENS } from "../../common/constants"
import { Button, Col, Progress, Row } from "reactstrap"
import AppHeader, { Body, Left, Right, Title } from "../../components/AppHeader"
import AppFooter from "../../components/AppFooter"
import { EntriesServices } from "../../common/APIServices"
import { API_BASE } from '../../config'
import moment from 'moment'
import Loader from "../../components/Loader"
import Swal from "sweetalert2"


const EntriesView = ({ history, terminal, shift, updateValue }) => {
    let [entries, setEntries] = useState([])
    let [progress, setProgress] = useState(-1)
    const [loading, setLoading] = useState(false)
    let inputElm = useRef(null)

    const getEntries = () => {
        var config = {
            params: {
                qs: {
                    _where: {
                        createdAt: {
                            gte: moment().startOf('day').toDate()
                        }
                    }
                }
            }
        }
        EntriesServices.all(config).then(res => {
            console.log(entries)
            setEntries(res.data)
        })
    }
    useEffect(() => {
        getEntries()
    }, []) // eslint-disable-line

    const handleClick = () => {
        inputElm.current.click()
    }

    const handleFileChange = e => {
        setLoading(true)
        let [file] = e.target.files
        let formData = new FormData()
        formData.append('file', file)
        formData.append('terminalId', terminal.id)
        formData.append('shiftId', shift.id)
        let config = {
            onUploadProgress: progressEvent => {
                let { loaded, total } = progressEvent
                let percentage = Math.round((loaded / total) * 100)
                setProgress(percentage)
            }
        }
        EntriesServices.getContainerDetails(formData, config).then(({ status, data }) => {
            setEntries([data, ...entries])
            setLoading(false)
            setProgress(-1)
            Swal.fire('Entry Success', `Container number is ${data.containerId || 'Not Recognized'}`, 'success')
        }).catch(err => {
            setLoading(false)
            setProgress(-1)
        })
        e.target.value = ''
    }

    return (
        <>
            <AppHeader className="justify-content-between">
                <Left className="icon" onClick={() => history.push(SCREENS.SHIFTS)}>
                    <i className="fas fa-arrow-left fa-lg"></i>
                </Left>
                <Body className="align-items-center">
                    <Title>{shift.name}</Title>
                </Body>
                <Right className="icon" />
            </AppHeader>
            <AppBody className="d-flex flex-column p-2">

                {
                    entries.map((item, inx) => {
                        return (
                            <Row className="m-0 bg-white mb-2 rounded entry" key={inx}>
                                <Col xs={4} className="img" style={{ backgroundImage: `url(${API_BASE}${item.image.path.replace(item.image.name, 'thumbnail-' + item.image.name)})` }}>
                                </Col>
                                <Col xs={8} className="py-3">
                                    <div className="h4">{item.containerId || 'Not Recognized'}</div>
                                    <div className="text-secondary">{moment(item.createdAt).format('DD MMM YYYY')}</div>
                                </Col>
                            </Row>
                        )
                    })
                }
                {
                    progress !== -1 && (
                        <Progress color="success" className="mt-auto" animated value={progress} >{progress}%</Progress>
                    )
                }
            </AppBody>
            <AppFooter>
                <input ref={inputElm} type="file" accept="image/*" capture="environment" className="image-inp" onChange={handleFileChange} />
                <Button disabled={loading} color="primary" className="rounded-0" size="lg" block onClick={handleClick}>{loading ? <Loader inverse /> : 'New Entry'} </Button>
            </AppFooter>
        </>
    )
}

const stateToProps = ({ terminal, shift }) => ({
    terminal, shift
})

const dispatchToProps = {
    updateValue
}

export default connect(stateToProps, dispatchToProps)(EntriesView)