import AppBody from "../../components/AppBody"
import AppFooter from "../../components/AppFooter"
import { Button } from 'reactstrap'
import { SCREENS } from "../../common/constants"


const WelcomeView = ({ history, ...props }) => {
    return (
        <>
            <AppBody className="d-flex flex-column">
                <div>
                    <img src="./assets/images/image-1.png" className="img-fluid" alt="" />
                </div>
                <div className="d-flex flex flex-column justify-content-around">
                    <div className="text-center">
                        <div className="h3">Container ID Reader</div>
                        <div className="mx-5">Your terminal is not registered your. Please register with your mobile number to capture container data.</div>
                    </div>
                </div>
            </AppBody>
            <AppFooter>
                <Button block size="lg" color="primary" onClick={() => history.push(SCREENS.ONBOARD)} className="rounded-0">Get Started</Button>
            </AppFooter>
        </>
    )
}


export default WelcomeView