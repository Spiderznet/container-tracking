import AppBody from "../../components/AppBody"
import AppFooter from "../../components/AppFooter"
import { Button, FormFeedback, FormGroup, Input } from 'reactstrap'
import { useFormik } from 'formik'
import { AuthServices } from "../../common/APIServices"
import { LOCAL_STORAGE, SCREENS } from "../../common/constants"
import * as yup from 'yup'
import { useEffect, useState } from "react"
import Swal from "sweetalert2"


const OnboardView = ({ history }) => {

    let [list, setList] = useState([])

    useEffect(() => {
        let token = localStorage.getItem(LOCAL_STORAGE.TOKEN)
        if (token) {
            history.push(SCREENS.TERMINALS)
        }
    }, []) // eslint-disable-line

    const { getFieldProps, errors, touched, isSubmitting, handleSubmit } = useFormik({
        initialValues: {
            username: ''
        },
        validationSchema: yup.object().shape({
            username: yup.string().required(),
        }),
        onSubmit: (values, { setSubmitting }) => {
            AuthServices.getDrivers(values.username).then(({ data }) => {
                setList(data)
                setSubmitting(false)
                if (data.length === 0) {
                    Swal.fire('Sorry', 'Phone number not found', 'error')
                }
            }).catch(() => {
                setSubmitting(false)
            })
        }
    })
    const handleDriver = (item) => {
        localStorage.setItem(LOCAL_STORAGE.TOKEN, item.token)
        history.push(SCREENS.TERMINALS)

    }

    return (
        <>
            <AppBody className="d-flex flex-column px-3 justify-content-center">
                <div className="text-center mb-4">
                    <div className="h2">Welcome Onboard!</div>
                    <div>Let’s help you meet up your tasks</div>
                </div>
                <div>
                    {
                        list.length === 0 && (
                            <FormGroup>
                                <Input
                                    placeholder="Enter mobile number"
                                    bsSize="lg"
                                    {...getFieldProps('username')}
                                    invalid={Boolean(errors.username && touched.username)}
                                />
                                <FormFeedback>{errors.username}</FormFeedback>
                            </FormGroup>
                        )
                    }

                    {
                        list.length !== 0 && (
                            <div>
                                <div className="h4 text-center">Select User</div>
                                {
                                    list.map((item, inx) => {
                                        return (
                                            <Button
                                                color="primary"
                                                key={inx}
                                                block
                                                className="rounded-0"
                                                size="lg"
                                                onClick={() => handleDriver(item)}
                                            >{item.name}</Button>
                                        )
                                    })
                                }
                            </div>
                        )
                    }
                </div>
            </AppBody>
            <AppFooter>
                {list.length === 0 && (
                    <Button block size="lg" color="primary" className="rounded-0" disabled={isSubmitting} onClick={handleSubmit}>Register</Button>
                )}
                {list.length !== 0 && (
                    <Button block size="lg" color="primary" className="rounded-0" onClick={() => setList([])}>Back</Button>
                )}

            </AppFooter>
        </>
    )
}


export default OnboardView