import AppBody from "../../components/AppBody"
import { ShiftsServices } from "../../common/APIServices"
import { useEffect, useState } from "react"
import { connect } from "react-redux"
import { updateValue } from "../../redux/actions"
import { SCREENS } from "../../common/constants"
import { Button } from "reactstrap"
import AppHeader, { Body, Left, Right, Title } from "../../components/AppHeader"


const ShiftsView = ({ history, terminal, updateValue }) => {

    let [shifts, setShifts] = useState([])
    
    useEffect(() => {
        ShiftsServices.all().then(res => {
            console.log(res)
            setShifts(res.data)
        })
    }, [])


    const handleShift = (shift) => {
        updateValue({
            shift
        })
        history.push(SCREENS.ENTRIES)
    }



    return (
        <>
            <AppHeader className="justify-content-between">
                <Left className="icon" onClick={() => history.push(SCREENS.TERMINALS)}>
                    <i className="fas fa-arrow-left fa-lg"></i>
                </Left>
                <Body className="align-items-center">
                    <Title>Shift</Title>
                </Body>
                <Right className="icon" />
            </AppHeader>
            <AppBody className="d-flex flex-column px-3 justify-content-center">
                <div className="text-center mb-4">
                    <div className="h4 mx-5">Container Tracking Terminal Number</div>
                    <div className="h1">{terminal.name}</div>
                    <div className="h4">Location</div>
                    <div className="h3">{terminal.location}</div>
                </div>
                <div>
                    <div className="h4 text-center">Select Shift</div>
                    {
                        shifts.map((item, inx) => {
                            return (
                                <Button
                                    color="primary"
                                    key={inx}
                                    block
                                    className="rounded-0"
                                    size="lg"
                                    onClick={() => handleShift(item)}
                                >{item.name}</Button>
                            )
                        })
                    }
                </div>
            </AppBody>
        </>
    )
}

const stateToProps = ({ terminal }) => ({
    terminal
})

const dispatchToProps = {
    updateValue
}

export default connect(stateToProps, dispatchToProps)(ShiftsView)