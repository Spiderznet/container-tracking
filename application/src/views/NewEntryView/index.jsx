import AppBody from "../../components/AppBody"
import { useEffect, useRef, useState } from "react"
import { connect } from "react-redux"
import { updateValue } from "../../redux/actions"
import { SCREENS } from "../../common/constants"
import { Button } from "reactstrap"
import AppHeader, { Body, Left, Right, Title } from "../../components/AppHeader"
import AppFooter from "../../components/AppFooter"
import Swal from "sweetalert2"
import { EntriesServices } from "../../common/APIServices"


const NewEntryView = ({ history, terminal, shift, updateValue }) => {


    const videoEle = useRef(null);
    const canvasEle = useRef(null);

    const [imageURL, setimageURL] = useState('');
    const [loading, setLoading] = useState(false)
    // const [docFile, setDocFile] = useState(null);

    const startCamera = async () => {
        try {
            if (!videoEle.current) return
            let appBodyElm = document.querySelector('.app-body')
            let aspectRatio = appBodyElm ? appBodyElm.clientHeight / appBodyElm.clientWidth : 1
            const stream = await navigator.mediaDevices.getUserMedia({
                video: {
                    facingMode: 'environment',
                    aspectRatio,
                }
            });
            if (!videoEle) return
            videoEle.current.srcObject = stream;

        } catch (err) {
            if (err.message === "Permission denied") {
                Swal.fire('Sorry', 'Please give camera access to take a photo', 'error')
            }
            else {
                Swal.fire('Sorry', err.message, 'error')
            }
            console.log(err);
        }
    }

    const stopCam = () => {
        if (!videoEle.current) return
        const stream = videoEle.current.srcObject;
        const tracks = stream.getTracks();

        tracks.forEach(track => {
            track.stop();
        });
    }

    const captureImage = async () => {
        // Get the exact size of the video element.
        const width = videoEle.current.videoWidth;
        const height = videoEle.current.videoHeight;

        // get the context object of hidden canvas
        const ctx = canvasEle.current.getContext('2d');

        // Set the canvas to the same dimensions as the video.
        canvasEle.current.width = width;
        canvasEle.current.height = height;
        ctx.drawImage(videoEle.current, 0, 0, width, height);
        const imageDataURL = canvasEle.current.toDataURL('image/jpeg');
        canvasEle.current.toBlob(blob => {
            let formData = new FormData()
            let file = new File([blob], 'image.jpg', { type: 'image/jpeg' })
            formData.append('file', file)
            setLoading(true)
            EntriesServices.getContainerDetails(formData).then(res => {
                console.log(res)
                setLoading(false)
            }).catch(err => {
                setLoading(false)
            })

        }, "image/jpeg", 1);
        stopCam();
        setimageURL(imageDataURL)
        // stop camera access
        videoEle.current.srcObject.getTracks().forEach(function (track) {
            track.stop();
        });

    }


    useEffect(() => {
        startCamera()
    }, [])


    return (
        <>
            <AppHeader className="justify-content-between">
                <Left className="icon" onClick={() => history.push(SCREENS.ENTRIES)}>
                    <i className="fas fa-arrow-left fa-lg"></i>
                </Left>
                <Body className="align-items-center">
                    <Title>New Entry</Title>
                </Body>
                <Right className="icon" />
            </AppHeader>
            <AppBody className="d-flex flex-column justify-content-center">
                {
                    imageURL ? (
                        <div className="preview h-100" style={{ backgroundImage: `url(${imageURL})` }}>
                            {loading && <div className="loader-wrapper">
                                <div className="loader fa-spin"></div>
                                <div className="mt-2 h5 text-white">Please Wait..</div>
                            </div>}
                        </div>
                    ) : (
                        <video className="camera" autoPlay={true} playsInline ref={videoEle}></video>
                    )
                }
            </AppBody>
            <AppFooter>
                <Button color="primary" className="rounded-0" size="lg" block onClick={captureImage}>Capture</Button>
            </AppFooter>
            <canvas ref={canvasEle} style={{ display: 'none' }}></canvas>
        </>
    )
}

const stateToProps = ({ terminal, shift }) => ({
    terminal, shift
})

const dispatchToProps = {
    updateValue
}

export default connect(stateToProps, dispatchToProps)(NewEntryView)