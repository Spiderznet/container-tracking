import AppBody from "../../components/AppBody"
import AppFooter from "../../components/AppFooter"
import { Button, FormFeedback, FormGroup, Input, Label } from 'reactstrap'
import { useFormik } from 'formik'
import { TerminalServices } from "../../common/APIServices"
import { useEffect, useState } from "react"
import { SCREENS } from "../../common/constants"
import { updateValue } from "../../redux/actions"
import { connect } from 'react-redux'
import * as yup from 'yup'


const TerminalsView = ({ history, updateValue }) => {

    let [terminals, setTerminals] = useState([])

    useEffect(() => {
        TerminalServices.all().then(res => {
            setTerminals(res.data)
        })
    }, [])

    const { getFieldProps, errors, touched, isSubmitting, handleSubmit } = useFormik({
        initialValues: {
            terminal: '',
        },
        validationSchema: yup.object().shape({
            terminal: yup.string().required()
        }),
        onSubmit: (values) => {
            console.log(values)
            updateValue({
                terminal: terminals.find(f => f.id.toString() === values.terminal)
            })
            history.push(SCREENS.SHIFTS)
        }
    })

    return (
        <>
            <AppBody className="d-flex flex-column px-3 justify-content-center">
                <div className="text-center mb-4">
                    <div className="h2">Terminals</div>
                </div>
                <div>
                    <FormGroup className="text-center">
                        <Label>Select Terminal</Label>
                        <Input
                            type="select"
                            bsSize="lg"
                            {...getFieldProps('terminal')}
                            invalid={Boolean(errors.terminal && touched.terminal)}
                        >
                            <option>Select Terminal</option>
                            {terminals.map((item, inx) => {
                                return (
                                    <option value={item.id} key={inx}>{item.name} {item.location}</option>
                                )
                            })}
                        </Input>
                        <FormFeedback>{errors.terminal}</FormFeedback>
                    </FormGroup>
                </div>
            </AppBody>
            <AppFooter>
                <Button block size="lg" color="primary" className="rounded-0" disabled={isSubmitting} onClick={handleSubmit}>Register</Button>
            </AppFooter>
        </>
    )
}

const stateToProps = state => ({

})

const dispatchToProps = {
    updateValue
}

export default connect(stateToProps, dispatchToProps)(TerminalsView)