/* eslint-disable no-useless-escape */
/* eslint-disable no-undef */
/* eslint-disable no-restricted-globals */
importScripts("https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js");
workbox.setConfig({ debug: false });

workbox.loadModule('workbox-strategies');
workbox.loadModule('workbox-expiration');


const { precacheAndRoute } = workbox.precaching;
const { cacheNames, setCacheNameDetails, skipWaiting, clientsClaim } = workbox.core;
const { registerRoute } = workbox.routing;
const { CacheFirst } = workbox.strategies;
const { ExpirationPlugin } = workbox.expiration;

skipWaiting();
clientsClaim();


// cache name
setCacheNameDetails({
    prefix: 'container-tracking',
    suffix: 'v1',
    precache: 'install-time',
    runtime: 'run-time',
    googleAnalytics: 'ga',
});


precacheAndRoute([...self.__WB_MANIFEST]);