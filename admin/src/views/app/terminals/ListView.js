import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { TerminalsServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';

const ListView = ({ history, match, ...props }) => {

    let [list, setList] = useState([])

    let name = "Terminals";

    const getList = () => {

        TerminalsServices.list().then(({ status, data }) => {
            if (status) {
                setList(data)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        TerminalsServices.deleteTask(value.task_id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const cols = React.useMemo(() => [
        {
            Header: 'ID',
            accessor: 'id',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Name',
            accessor: 'name',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Location',
            accessor: 'location',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Action',
            accessor: 'action',
            cellClass: 'text-nowrap text-muted',
            Cell: ({ row }) => {
                return (
                    <React.Fragment>
                        <Link to={`${match.url}/view/${row.original.id}`} className="pr-2 pointer" >
                            <i className="simple-icon-eye" />
                        </Link>
                        <Link to={`${match.url}/edit/${row.original.id}`} className="pr-2 pointer" >
                            <i className="simple-icon-note" />
                        </Link>
                    </React.Fragment>
                )
            }
        },
    ], []);

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={name} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${match.url}/create`}>Add New</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={list} divided />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default ListView