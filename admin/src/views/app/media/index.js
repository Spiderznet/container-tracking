import React from 'react'
import { Row } from 'reactstrap'
import { Colxx, Separator } from '../../../components/common/CustomBootstrap'
import MediaList from '../../../components/MediaList'
import Breadcrumb from '../../../containers/navs/Breadcrumb'

const Media = ({ data, match, ...props }) => {
    let { title, name, path } = data
    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={title} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    {/* <Link className="btn btn-primary btn-lg" to={`${match.url}/create`}>Add New</Link> */}
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <MediaList />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default Media