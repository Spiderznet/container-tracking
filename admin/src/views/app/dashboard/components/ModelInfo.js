import React, { useEffect, useState } from 'react'
import IconCard from '../../../../components/cards/IconCard'
import { http } from '../../../../utils/APIServices'

const ModelInfo = ({ title, icon, path, history, ...props }) => {
    let [value, setValue] = useState(0)
    useEffect(() => {
        http.get(`${path}/count`).then(({ status, data }) => {
            if (status) {
                setValue(data)
            }
        })
    }, [])
    return (
        <IconCard history={history} title={title} icon={icon} value={value} path={path} />
    )
}

export default ModelInfo