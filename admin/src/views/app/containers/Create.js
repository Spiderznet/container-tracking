import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardTitle } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { useFormik } from 'formik'
import * as yup from 'yup'
import { ContainersServices } from '../../../utils/APIServices'
import { adminRoot } from '../../../constants/defaultValues';
import { stringify } from 'qs'
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { toFormData } from '../../../utils';
import FormGenerator from '../../../components/FormGenerator';

const Create = ({ match, ...props }) => {
    let { id = "" } = match.params
    let [options, setOptions] = useState({})
    let name = "Containers";
    let path = "/containers"
    const [isLoading, setIsLoading] = useState(id ? true : false)

    const formSchema = yup.object().shape({
        id: yup.string()
            .required('id is required'),
    })



    useEffect(() => {
        // TerminalsServices.addInfo().then(({ status, data }) => {
        //     if (status) {
        //         setOptions({
        //             ...options,
        //             roles: data.roles.map(item => ({ name: item.name, id: item.id }))
        //         })
        //     }
        // })
        if (id) {
            ContainersServices.details(id).then(({ status, data }) => {
                if (status) {

                    setValues({
                        id: data.id,
                    })
                    setIsLoading(false)
                }
            })
        }
    }, [])

    const formProps = useFormik({
        initialValues: {
            id: '',
        },
        validationSchema: formSchema,
        onSubmit: ({ ...values }, { setSubmitting, resetForm, setFieldError }) => {
            let formData = new FormData();

            // formData = toFormData(values, formData)
            formData = values


            if (id) {
                ContainersServices.update(id, formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}${path}/view/${data.id}`)
                    }
                }).finally(res => setSubmitting(false))
            } else {
                ContainersServices.insert(formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}${path}/view/${data.id}`)
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating } = formProps

    if (isLoading) {
        return <div className="loading" />
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                search
            }, { encode: false })
        }
        ContainersServices.userList(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }

    let fields = [
        { name: 'id', label: 'Container No' },
    ]

    return (
        <React.Fragment>
            <Breadcrumb heading={id ? "Edit " + name : "Create " + name} match={match} />
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <CardTitle>
                                <i className="simple-icon-people mr-2"></i>{name} Detail Form
                            </CardTitle>
                            <FormGenerator
                                fields={fields}
                                {...formProps}
                            />
                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default Create