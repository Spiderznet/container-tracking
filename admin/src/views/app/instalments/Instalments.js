import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row, Modal, ModalHeader, ModalBody, Col } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { InstalmentsServices, LoanServices, UserServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { formatAmount } from '../../../utils';
import FormGenerator from '../../../components/FormGenerator';
import * as yup from 'yup'
import { useFormik } from 'formik';
import { stringify } from 'qs';
import moment from 'moment'
import { getCurrentUser } from '../../../helpers/Utils';
import { UserRole } from '../../../helpers/authHelper';

const Instalments = ({ history, match, ...props }) => {

    let [list, setList] = useState([])
    let [isLoading, setIsloading] = useState(false)
    let [isprocess, setIsprocess] = useState(false)
    let [isOpen, setIsOpen] = useState(false)
    let [preview, setPreview] = useState(null)
    let userDetails = getCurrentUser()

    let name = "Instalments";

    const getList = () => {
        setIsloading(true)
        let query = ''
        let _where = {}
        if (userDetails.role != UserRole.root) {
            _where = {
                ..._where,
                collector_id_eq: userDetails.user_id
            }
        }
        query = "?" + stringify({
            _where,
            _sort: {
                created_at: "DESC"
            }
        }, { encode: false })


        InstalmentsServices.list(query).then(({ status, data }) => {
            if (status) {
                setList(data)
                setIsloading(false)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        InstalmentsServices.delete(value.id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const cols = React.useMemo(() => [
        {
            Header: 'ID',
            accessor: 'id',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Loan Id',
            accessor: 'loan_id',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <div className={`${props.value > 0 ? 'text-success' : 'text-danger'}`}>{formatAmount(props.value)}</div>,
        },
        {
            Header: 'Created At',
            accessor: 'created_at',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{moment(props.value).format('MMMM Do YYYY, h:mm:ss a')}</>,
        },
    ], []);

    const formSchema = yup.object().shape({
        loan_id: yup.mixed()
            .test('required', "Loan Number is required", val => val && val.name),

    })


    const formProps = useFormik({
        initialValues: {
            loan_id: '',
        },
        validationSchema: formSchema,
        onSubmit: ({ id = "", ...values }, { setSubmitting, resetForm, setFieldError }) => {
            let params = {
                loan_id: values.loan_id.id,
            }
            if (id) {
                InstalmentsServices.update(id, params).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            } else {
                InstalmentsServices.insert(params).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating, resetForm } = formProps

    const toggle = () => {
        setIsOpen(!isOpen)
        resetForm()
        setPreview(null)
    }

    const getDetails = (id) => {
        setIsprocess(true)
        toggle();
        InstalmentsServices.details(id).then(({ status, data }) => {
            if (status) {
                setValues({
                    user_id: data.user_id.id,
                    amount: data.amount,
                    description: data.description,
                })
                setPreview(data.slide)
                setIsprocess(false)
            }
        })
    }

    const loadLoans = (search, callback) => {
        let query = ''
        if (search) {
            let _where = {
                status_eq: 'IN_PROGRESS',
            }
            if (![UserRole.root, UserRole.Admin].includes(userDetails.role)) {
                _where['collector_id_eq'] = userDetails.user_id
            }
            query = "?" + stringify({
                _where,
                search
            }, { encode: false })
        }
        LoanServices.list(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `Loan no - ${item.id}`,
                    ...item,
                }))
                callback(data)
            }
        })
    }


    let fields = [
        {
            name: 'loan_id', label: 'Loan Number', type: "single-select-async", props: { loadOptions: loadLoans }, Info: ({ value }) => value ? (

                <Row>
                    <Col xs={5}><b>Name:</b></Col>
                    <Col xs={7}>{value.user.name}</Col>
                    <Col xs={5}><b>Instalment:</b></Col>
                    <Col xs={7}>{formatAmount(value.instalment_amount)}</Col>
                    <Col xs={5}><b>Instalment Paid:</b></Col>
                    <Col xs={7}>{value.instalments.length}/{value.no_of_instalment}</Col>
                </Row>
            ) : ''
        },
    ]

    if (isLoading) {
        return <div className="loading" />
    }
    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={name} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Button color="primary" size="lg" onClick={toggle}>Add New</Button>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable className="table-responsive" columns={cols} data={list} divided />
                </Colxx>
            </Row>
            <Modal wrapClassName="modal-right" isOpen={isOpen} toggle={toggle}>
                <ModalHeader toggle={toggle}>{values.id ? 'Update Details' : 'Add New'}</ModalHeader>
                <ModalBody>
                    {
                        isprocess ?
                            (
                                <div className="loading" />
                            ) : (
                                <FormGenerator
                                    submitLabel={'Collected'}
                                    fields={fields}
                                    {...formProps}
                                />
                            )
                    }
                </ModalBody>
            </Modal>
        </React.Fragment>
    )
}

export default Instalments