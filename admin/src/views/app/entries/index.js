import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const Create = React.lazy(() =>
    import(/* webpackChunkName: "create-entries" */ './Create')
);
const ListView = React.lazy(() =>
    import(/* webpackChunkName: "entries-list" */ './ListView')
);
const Details = React.lazy(() =>
    import(/* webpackChunkName: "entries-details" */ './Details')
);

const User = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/create`}
                render={(props) => <Create {...props} />}
            />
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <Details {...props} />}
            />
            <Route
                path={`${match.url}/edit/:id`}
                render={(props) => <Create {...props} />}
            />
            <Route
                path={`${match.url}/`}
                render={(props) => <ListView {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default User;
