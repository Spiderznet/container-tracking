import React, { useState, useEffect } from 'react'
import { EntriesServices } from '../../../utils/APIServices'
import ListGenerator from '../../../components/ListGenerator';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Separator, Colxx } from '../../../components/common/CustomBootstrap';
import { Row } from 'reactstrap';

const Details = ({ match, ...props }) => {

    let { id = "" } = match.params
    let name = "Entries";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [values, setValues] = useState({})
    useEffect(() => {
        if (id) {
            EntriesServices.details(id).then(({ status, data }) => {
                if (status) {
                    setValues(data)
                    setIsLoading(false)
                }
            })
        }
    }, [])

    let list = [
        { name: "id", label: 'Id' },
        { name: "containerId", label: 'Container Id' },
        {
            name: "image", label: 'image', Info: ({ value }) => <a className="d-inline-block" download href={value.path}>
                <img src={value.path} alt="" height="100" />
                <div>Download Image</div>
            </a>
        },
        { name: "shift", label: 'Shift', Info: ({ value }) => value.name },
        { name: "terminal", label: 'Terminal', Info: ({ value }) => value.name },
        { name: "user", label: 'User', Info: ({ value }) => value.name },
        { name: "createdAt", label: 'Created At' },
        { name: "updatedAt", label: 'Updated At' },

    ]

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>
            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={`${name} Details`} match={match} />
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <ListGenerator
                        list={list}
                        values={values}
                    />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}


export default Details