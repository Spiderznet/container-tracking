import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row, Modal, ModalHeader, ModalBody } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { PaymentsServices, UserServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { formatAmount } from '../../../utils';
import FormGenerator from '../../../components/FormGenerator';
import * as yup from 'yup'
import { useFormik } from 'formik';
import { stringify } from 'qs';
import moment from 'moment'
import { getCurrentUser } from '../../../helpers/Utils';
import { UserRole } from '../../../helpers/authHelper';
import { DateRangeColumnFilter, NumberRangeColumnFilter, SelectColumnFilter, SliderColumnFilter } from '../../../components/TableFilters';
import TransactionChart from './components/TransactionChart';

const Transactions = ({ history, match, ...props }) => {

    let [list, setList] = useState([])
    let [isLoading, setIsloading] = useState(false)
    let [isprocess, setIsprocess] = useState(false)
    let [isOpen, setIsOpen] = useState(false)
    let [preview, setPreview] = useState(null)
    let userDetails = getCurrentUser()


    let name = "Transactions";

    let mapTableData = (data = []) => data.map(item => ({
        ...item,
        user: `${item.user.name} (${item.user.id})`,
        created_at: new Date(item.created_at).getTime()
    }))

    const getList = () => {
        setIsloading(true)
        let query = ''
        let _where = {}
        if (![UserRole.root, UserRole.Admin].includes(userDetails.role)) {
            _where = {
                ..._where,
                user_id_eq: userDetails.user_id
            }
        }
        query = "?" + stringify({
            _where,
            _sort: {
                created_at: "DESC"
            }
        }, { encode: false })
        PaymentsServices.list(query).then(({ status, data }) => {
            if (status) {
                setList(
                    mapTableData(data)
                )
                setIsloading(false)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        PaymentsServices.delete(value.id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const cols = React.useMemo(() => [
        {
            Header: 'ID',
            accessor: 'id',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'User',
            accessor: 'user',
            cellClass: 'text-nowrap text-muted',
            Filter: SelectColumnFilter,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            cellClass: 'text-nowrap text-muted',
            Filter: NumberRangeColumnFilter,
            filter: 'between',
            Cell: (props) => <div className={`${props.value > 0 ? 'text-success' : 'text-danger'}`}>{formatAmount(props.value)}</div>,
        },
        {
            Header: 'Description',
            accessor: 'description',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Balance',
            accessor: 'balance',
            cellClass: 'text-nowrap text-muted',
            disableFilters: true,
            Cell: (props) => <>{formatAmount(props.value)}</>,
        },
        {
            Header: 'Created At',
            accessor: 'created_at',
            cellClass: 'text-nowrap text-muted',
            Filter: DateRangeColumnFilter,
            filter: 'between',
            Cell: (props) => {
                return (
                    <>{moment(props.value).format('MMMM Do YYYY, h:mm:ss a')}</>
                )
            },
        },
    ], []);

    const formSchema = yup.object().shape({
        user_id: yup.mixed()
            .test('required', "User is required", val => val && val.name),
        amount: yup.number()
            .typeError('Enter valid amount')
            .required('Amount is Requires')

    })


    const formProps = useFormik({
        initialValues: {
            user_id: '',
            amount: '',
            description: '',
        },
        validationSchema: formSchema,
        onSubmit: ({ id = "", ...values }, { setSubmitting, resetForm, setFieldError }) => {
            if (id) {
                PaymentsServices.update(id, values).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            } else {
                PaymentsServices.insert(values).then(({ status, data, msg }) => {
                    if (status) {
                        toggle()
                        getList()
                        NotificationManager.success('', msg, 3000, null, null, '');
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating, resetForm } = formProps

    const toggle = () => {
        setIsOpen(!isOpen)
        resetForm()
        setPreview(null)
    }

    const getDetails = (id) => {
        setIsprocess(true)
        toggle();
        PaymentsServices.details(id).then(({ status, data }) => {
            if (status) {
                setValues({
                    user_id: data.user_id.id,
                    amount: data.amount,
                    description: data.description,
                })
                setPreview(data.slide)
                setIsprocess(false)
            }
        })
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                _where: {
                    role_id_in: [1, 2, 3]
                },
                search
            }, { encode: false })
        }
        UserServices.list(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }


    let fields = [
        { name: 'user_id', label: 'User', type: "single-select-async", props: { loadOptions: loadUsers } },
        { name: 'amount', label: 'Amount' },
        { name: 'description', label: 'Description', type: "textarea" },
    ]

    if (isLoading) {
        return <div className="loading" />
    }
    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={name} match={match} />
                </Colxx>
                {
                    [UserRole.root, UserRole.Admin].includes(userDetails.role) && (
                        <Colxx md={4} className="text-right">
                            <Button color="primary" size="lg" onClick={toggle}>Add New</Button>
                        </Colxx>
                    )
                }
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <TransactionChart />
                    <ReactTable className="table-responsive" columns={cols} data={list} divided />
                </Colxx>
            </Row>
            <Modal wrapClassName="modal-right" isOpen={isOpen} toggle={toggle}>
                <ModalHeader toggle={toggle}>{values.id ? 'Update Details' : 'Add New'}</ModalHeader>
                <ModalBody>
                    {
                        isprocess ?
                            (
                                <div className="loading" />
                            ) : (
                                <FormGenerator
                                    fields={fields}
                                    {...formProps}
                                />
                            )
                    }
                </ModalBody>
            </Modal>
        </React.Fragment>
    )
}

export default Transactions