import React, { Suspense } from 'react';
import { withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from '../../layout/AppLayout';
import { ProtectedRoute, UserRole } from '../../helpers/authHelper';


const Dashboard = React.lazy(() =>
    import(/* webpackChunkName: "viwes-dashboard-page" */ './dashboard')
);

const Media = React.lazy(() =>
    import(/* webpackChunkName: "viwes-media-page" */ './media')
);

const ModelsView = React.lazy(() =>
    import(/* webpackChunkName: "views-models" */ '../../models')
);



const App = ({ models = [], match }) => {
    let media = models.find(f => f.name === 'media')
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <Suspense fallback={<div className="loading" />}>
                    <Switch>
                        <Redirect exact from={`${match.url}/`} to={`${match.url}/dashboard`} />
                        <ProtectedRoute
                            path={`${match.url}/dashboard`}
                            component={Dashboard}
                            roles={[UserRole.Admin, UserRole.root, UserRole.collector]}
                        />
                        {
                            models.filter(f => f.name !== "media").map((item, inx) => {
                                return (
                                    <ProtectedRoute
                                        path={match.url + item.path}
                                        component={ModelsView}
                                        roles={item.roles}
                                        // exact
                                        data={item}
                                        key={inx}
                                    />
                                )
                            })
                        }
                        {
                            media !== null && (
                                <ProtectedRoute
                                    path={`${match.url}${media.path}`}
                                    component={Media}
                                    roles={media.roles}
                                    data={media}
                                />
                            )
                        }

                        <Redirect to="/error" />
                    </Switch>
                </Suspense>
            </div>
        </AppLayout>
    );
};

const mapStateToProps = ({ menu, settings }) => {
    const { containerClassnames } = menu;
    const { models } = settings;
    return { containerClassnames, models };
};

export default withRouter(connect(mapStateToProps, {})(App));
