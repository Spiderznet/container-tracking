import React, { useState, useEffect } from 'react'
import { LoanServices, UserServices } from '../../../utils/APIServices'
import { adminRoot } from '../../../constants/defaultValues';
import ListGenerator from '../../../components/ListGenerator';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import { Separator, Colxx } from '../../../components/common/CustomBootstrap';
import { Row } from 'reactstrap';
import { formatAmount } from '../../../utils';

const UserDetails = ({ match, ...props }) => {

    let { id = "" } = match.params
    let name = "User";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [values, setValues] = useState({})
    useEffect(() => {
        if (id) {
            UserServices.details(id).then(({ status, data }) => {
                if (status) {
                    setValues(data)
                    setIsLoading(false)
                }
            })
        }
    }, [])

    let list = [
        { name: "id", label: 'User Id' },
        { name: "name", label: 'Name' },
        { name: "phone", label: 'Phone' },
        { name: "is_active", label: 'Status' },
        { name: "role", label: 'Role', Info: ({ value }) => value.name },
        { name: "image", label: 'Image' },
        { name: "created_at", label: 'Created At' },
        { name: "updated_at", label: 'Updated At' },

    ]

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>
            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={`${name} Details`} match={match} />
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <ListGenerator
                        list={list}
                        values={values}
                    />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}


export default UserDetails