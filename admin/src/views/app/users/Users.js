import React, { useEffect } from 'react';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { Button, Row } from 'reactstrap';
import ReactTable from '../../../components/ReactTable';
import { UserServices } from '../../../utils/APIServices';
import { useState } from 'react';
import { NotificationManager } from '../../../components/common/react-notifications';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { formatAmount } from '../../../utils';
import { adminRoot } from '../../../constants/defaultValues';
import { SelectColumnFilter } from '../../../components/TableFilters';

const Users = ({ history, match, ...props }) => {

    let [list, setList] = useState([])

    let name = "Users";

    const getList = () => {

        UserServices.list().then(({ status, data }) => {
            if (status) {
                setList(data)
            }
        })
    }

    useEffect(() => {
        getList()
    }, [])

    const deleteRow = (value) => {
        // console.log(value.id);
        UserServices.deleteTask(value.task_id).then(res => {
            if (res.status) {
                getList()
                NotificationManager.success('', res.msg, 3000, null, null, '');
            }
        })
    }

    const cols = React.useMemo(() => [
        {
            Header: 'ID',
            accessor: 'id',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Name',
            accessor: 'name',
            cellClass: 'text-nowrap text-muted',
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Phone',
            accessor: 'phone',
            cellClass: 'text-nowrap text-muted',
            Filter: SelectColumnFilter,
            Cell: (props) => <>{props.value}</>,
        },
        {
            Header: 'Status',
            accessor: 'isActive',
            cellClass: 'text-nowrap text-muted',
            Filter: SelectColumnFilter,
            Cell: (props) => props.value,
        },
        {
            Header: 'Action',
            accessor: 'action',
            cellClass: 'text-nowrap text-muted',
            Cell: ({ row }) => {
                return (
                    <React.Fragment>
                        <Link to={`${adminRoot}/users/view/${row.original.id}`} className="pr-2 pointer" >
                            <i className="simple-icon-eye" />
                        </Link>
                        <Link to={`${adminRoot}/users/edit/${row.original.id}`} className="pr-2 pointer" >
                            <i className="simple-icon-note" />
                        </Link>
                        {/* <span onClick={() => deleteRow(props.row.original)} className="pointer">
                            <i className="simple-icon-trash" />
                        </span> */}
                    </React.Fragment>
                )
            }
        },
    ], []);

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={name} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${match.url}/create`}>Add New</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <h3 className="mb-4">
                    </h3>
                </Colxx>

                <Colxx xxs="12">
                    <ReactTable columns={cols} data={list} divided />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default Users