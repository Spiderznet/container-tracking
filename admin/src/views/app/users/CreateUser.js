import React, { useState, useEffect } from 'react';
import { Row, Card, CardBody, CardTitle } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import { useFormik } from 'formik'
import * as yup from 'yup'
import { UserServices } from '../../../utils/APIServices'
import { adminRoot } from '../../../constants/defaultValues';
import { stringify } from 'qs'
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { toFormData } from '../../../utils';
import FormGenerator from '../../../components/FormGenerator';

const CreateUser = ({ match, ...props }) => {
    let { id = "" } = match.params
    let [options, setOptions] = useState({})
    let name = "User";
    const [isLoading, setIsLoading] = useState(id ? true : false)

    const formSchema = yup.object().shape({
        name: yup.string()
            .required('Name is required'),
        ...(id === '' ? {
            password: yup.string()
                .required('Password is required')
                .min(8)
                .max(15)
                .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])/, "password Must contain 1 Uppercase, 1 Lowercase, 1 Number and 1 special character"),
        } : {}),
        role_id: yup.mixed()
            .test('required', 'Role is Required', val => val && val.name),
        phone: yup.string().required('Phone is required'),
        image: yup.mixed().test({
            name: 'fileSize',
            message: 'File Size is too large',
            test: file => file ? file[0].size <= 1e+6 : true
        }),
    })



    useEffect(() => {
        // UserServices.addInfo().then(({ status, data }) => {
        //     if (status) {
        //         setOptions({
        //             ...options,
        //             roles: data.roles.map(item => ({ name: item.name, id: item.id }))
        //         })
        //     }
        // })
        if (id) {
            UserServices.details(id).then(({ status, data }) => {
                if (status) {
                    setValues({
                        name: data.name,
                        role_id: { id: data.role.id, name: data.role.name },
                        phone: data.phone,
                    })
                    setIsLoading(false)
                }
            })
        }
    }, [])

    const formProps = useFormik({
        initialValues: {
            name: '',
            password: '',
            role_id: '',
            phone: '',
            image: '',
            is_active: 'ACTIVE'
        },
        validationSchema: formSchema,
        onSubmit: ({ image, ...values }, { setSubmitting, resetForm, setFieldError }) => {
            let formData = new FormData();

            if (image && image.length) {
                values.image = image[0]
            }

            formData = toFormData(values, formData)


            if (id) {
                UserServices.update(id, formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/users/view/${data.id}`)
                    }
                }).finally(res => setSubmitting(false))
            } else {
                UserServices.insert(formData).then(({ status, data }) => {
                    if (status) {
                        resetForm()
                        props.history.push(`${adminRoot}/users/view/${data.id}`)
                    }
                }).finally(res => setSubmitting(false))
            }

        }
    })
    const { values, getFieldProps, handleSubmit, errors, touched, setFieldValue, setFieldTouched, setValues, isSubmitting, isValidating } = formProps

    if (isLoading) {
        return <div className="loading" />
    }

    const loadUsers = (search, callback) => {
        let query = ''
        if (search) {
            query = "?" + stringify({
                search
            }, { encode: false })
        }
        UserServices.userList(query).then(({ status, data, msg }) => {
            if (status && Array.isArray(data)) {
                data = data.map(item => ({
                    id: item.id,
                    name: `${item.id} - ${item.name}(${item.phone})`
                }))
                callback(data)
            }
        })
    }

    let fields = [
        { name: 'name', label: 'Name' },
        { name: 'phone', label: 'Phone' },
        { name: 'password', label: 'Password', type: 'password' },
        { name: 'role_id', label: 'Role', type: 'single-select', props: { options: options.roles || [] } },
        { name: 'image', label: 'Image', type: 'file', props: { accept: "image/*" } },
    ]

    return (
        <React.Fragment>
            <Breadcrumb heading={id ? "Edit " + name : "Create " + name} match={match} />
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card>
                        <CardBody>
                            <CardTitle>
                                <i className="simple-icon-people mr-2"></i>{name} Detail Form
                            </CardTitle>
                            <FormGenerator
                                fields={fields}
                                {...formProps}
                            />
                        </CardBody>
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

export default CreateUser