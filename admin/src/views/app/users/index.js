import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const CreateUser = React.lazy(() =>
    import(/* webpackChunkName: "create-user" */ './CreateUser')
);
const Users = React.lazy(() =>
    import(/* webpackChunkName: "user-list" */ './Users')
);
const UserDetails = React.lazy(() =>
    import(/* webpackChunkName: "user-details" */ './UserDetails')
);

const User = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/create`}
                render={(props) => <CreateUser {...props} />}
            />
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <UserDetails {...props} />}
            />
            <Route
                path={`${match.url}/edit/:id`}
                render={(props) => <CreateUser {...props} />}
            />
            <Route
                path={`${match.url}/`}
                render={(props) => <Users {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default User;
