import React, { useState, useEffect } from 'react'
import { LoanServices } from '../../../utils/APIServices'
import { adminRoot } from '../../../constants/defaultValues';
import ListGenerator from '../../../components/ListGenerator';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import { Separator, Colxx } from '../../../components/common/CustomBootstrap';
import { Row } from 'reactstrap';
import { formatAmount } from '../../../utils';

const LoanDetails = ({ match, ...props }) => {

    let { id = "" } = match.params
    let name = "Loan";
    const [isLoading, setIsLoading] = useState(id ? true : false)
    const [values, setValues] = useState({})
    useEffect(() => {
        if (id) {
            LoanServices.details(id).then(({ status, data }) => {
                if (status) {
                    setValues(data)
                    setIsLoading(false)
                }
            })
        }
    }, [])

    let list = [
        { name: "id", label: 'Loan Number' },
        { name: 'user', label: 'User', Info: ({ value }) => value.name },
        { name: 'collector', label: 'Collector', Info: ({ value }) => value.name },
        { name: "principal_amount", label: 'Principal Amount', Info: ({ value }) => formatAmount(value) },
        { name: "intrest", label: 'Intrest', Info: ({ value }) => `${value} (${values.interest_type})` },
        { name: "intrest_amount", label: 'Intrest Amount', Info: ({ value }) => formatAmount(value) },

        { name: "total_amount", label: 'Total Amount', Info: ({ value }) => formatAmount(value) },
        { name: "duration", label: 'Duration', Info: ({ value }) => `${value} (${values.duration_type})` },

        { name: "instalment_cycle", label: 'Instalment Cycle', Info: ({ value }) => `${value} (${values.instalment_cycle_type})` },
        { name: "no_of_instalment", label: 'No Of Instalment' },
        { name: "instalment_amount", label: 'Instalment Amount', Info: ({ value }) => formatAmount(value) },
        { name: "status", label: 'Status' },
        { name: "created_at", label: 'Created At', type: 'date' },
    ]

    if (isLoading) {
        return <div className="loading" />
    }

    return (
        <React.Fragment>
            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={`${name} Details`} match={match} />
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    <ListGenerator
                        list={list}
                        values={values}
                    />
                </Colxx>
            </Row>
        </React.Fragment>
    )
}


export default LoanDetails