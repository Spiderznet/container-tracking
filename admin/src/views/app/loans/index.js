import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const CreateLoan = React.lazy(() =>
    import(/* webpackChunkName: "create-loan" */ './CreateLoan')
);

const LoanDetails = React.lazy(() =>
    import(/* webpackChunkName: "loan-details" */ './LoanDetails')
);
const Loans = React.lazy(() =>
    import(/* webpackChunkName: "loan-list" */ './Loans')
);

const User = ({ match }) => (
    <Suspense fallback={<div className="loading" />}>
        <Switch>
            <Route
                path={`${match.url}/create`}
                render={(props) => <CreateLoan {...props} />}
            />
            <Route
                path={`${match.url}/edit/:id`}
                render={(props) => <CreateLoan {...props} />}
            />
            <Route
                path={`${match.url}/view/:id`}
                render={(props) => <LoanDetails {...props} />}
            />
            <Route
                path={`${match.url}/`}
                render={(props) => <Loans {...props} />}
            />

            <Redirect to="/error" />
        </Switch>
    </Suspense>
);
export default User;
