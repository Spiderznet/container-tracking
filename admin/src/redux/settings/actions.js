import { CHANGE_LOCALE, SET_ENUM_LIST, SET_MODELS_LIST } from '../actions';
import { setCurrentLanguage } from '../../helpers/Utils';

export const changeLocale = (locale) => {
  setCurrentLanguage(locale);
  return {
    type: CHANGE_LOCALE,
    payload: locale,
  };
};


export const setModelsList = payload => ({
  type: SET_MODELS_LIST,
  payload
})

export const setEnumList = payload => ({
  type: SET_ENUM_LIST,
  payload
})
