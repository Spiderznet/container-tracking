import { CHANGE_LOCALE, SET_ENUM_LIST, SET_MODELS_LIST } from '../actions';
import { getCurrentLanguage } from '../../helpers/Utils';

const INIT_STATE = {
  locale: getCurrentLanguage(),
  models: [],
  enums: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case CHANGE_LOCALE:
      return { ...state, locale: action.payload };
    case SET_MODELS_LIST:
      return { ...state, models: [...state.models, ...action.payload] }
    case SET_ENUM_LIST:
      return { ...state, enums: [...state.enums, ...action.payload] }
    default:
      return { ...state };
  }
};
