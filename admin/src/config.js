export const API_ROOT = process.env.REACT_APP_API_ROOT
export const API_BASE = process.env.REACT_APP_API_BASE
export const TINY_KEY = process.env.REACT_APP_TINY_KEY

export const DEFAULT_PROFILE = '/assets/img/profiles/profile.png'