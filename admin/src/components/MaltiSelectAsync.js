import React from 'react'
import Select from 'react-select/async'

const MaltiSelectAsync = ({ invalid = false, className = '', name, onChange, onBlur, ...props }) => {
    const handleChange = (val) => {
        onChange(name, val);
    };

    const handleBlur = () => {
        onBlur(name, true);
    };

    return (
        <Select
            className={`react-select ${invalid ? 'is-invalid' : ''} ${className} ${name ? 'field-' + name : ''}`}
            classNamePrefix="react-select"
            getOptionLabel={option => option.name}
            getOptionValue={option => option.id}
            isMulti
            onChange={handleChange}
            onBlur={handleBlur}
            {...props}
        />
    )
}

export default MaltiSelectAsync