import React from 'react';
import { Card, CardBody } from 'reactstrap';
import { adminRoot } from '../../constants/defaultValues';

const IconCard = ({ className = 'mb-2', icon, title, value, history, path }) => {
  return (
    <div onClick={() => history.push(adminRoot + path)} className={`icon-row-item ${className}`}>
      <Card>
        <CardBody className="text-center">
          <i className={icon} />
          <p className="card-text font-weight-semibold mb-0">
            {title}
          </p>
          <p className="lead text-center">{value}</p>
        </CardBody>
      </Card>
    </div>
  );
};

export default React.memo(IconCard);
