import React, { useEffect, useRef, useState } from 'react'
import { connect } from 'react-redux'
import { Button, Col, Input, Label, Pagination, PaginationItem, PaginationLink, Progress, Row } from 'reactstrap'
import { http } from '../utils/APIServices'
import DragAndDrop from './DragAndDrop'
import moment from 'moment';
import { API_BASE } from '../config'
import Swal from 'sweetalert2'

const MediaList = ({ models, allowSelect = false, selected = null, onSelect = () => { }, ...props }) => {
    let MediaModel = models.find(f => f.title === 'Media')
    let { path, fields = [], title } = MediaModel
    let [progress, setProgress] = useState(-1)
    const [list, setList] = useState([])
    const [uploadFile, setUploadFile] = useState(null)
    const uploadInp = useRef(null)
    let [take, setTake] = useState('10')
    let [skip, setSkip] = useState(0)
    let [paginationItems, setPaginationItems] = useState([])
    let [total, setTotal] = useState(0)

    const getList = () => {
        let query = JSON.stringify({
            _sort: {
                createdAt: "desc"
            },
            _limit: Number(take),
            _offset: skip * Number(take)
        })
        let config = {
            params: {
                qs: query
            }
        }
        http.get(path, config).then(({ status, data }) => {
            if (status) {
                setList(data)
            }
        })
    }

    useEffect(() => {

        http.get(path + '/count').then(({ status, data }) => {
            if (status) {
                setTotal(data)
            }
        })
    }, [])

    useEffect(() => {
        getList()
    }, [take, skip])

    useEffect(() => {
        let pages = Math.ceil(total / Number(take))
        pages = Array(pages).fill().map((x, i) => i)
        setPaginationItems(pages)
    }, [take, total])

    const handleFile = (files) => {
        let [file] = files
        let formData = new FormData()
        formData.append('file', file)
        setUploadFile(file)
        let fr = new FileReader()
        fr.onload = ({ target }) => {
            setUploadFile({
                name: file.name,
                url: target.result
            })
        }
        fr.readAsDataURL(file);
        let config = {
            onUploadProgress: progressEvent => {
                let { loaded, total } = progressEvent
                let percentage = Math.round((loaded / total) * 100)
                setProgress(percentage)
            }
        }
        http.post(path, formData, config).then(({ status, data }) => {
            if (status) {
                setList([data, ...list])
                setProgress(-1)
                setUploadFile(null)
            }
        })
    }

    const handleUpload = ({ target }) => {
        let { files } = target
        handleFile(files)
    }

    const deleteRow = (row) => {
        Swal.fire({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this.",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(({ isConfirmed }) => {
            if (isConfirmed) {
                http.delete(`${path}/${row.id}`).then(({ status, data }) => {
                    if (status) {
                        getList()
                        Swal.fire('Success', 'Deleted Successfuly', 'success')
                    }
                })
            }
        })
    }

    return (
        <React.Fragment>
            <div className="text-right mb-3">
                <div className="upload-button">
                    <Button color="primary" onClick={() => uploadInp.current.click()}><i className="simple-icon-cloud-upload" /> Upload</Button>
                    <input type="file" ref={uploadInp} onChange={handleUpload} className="upload-input" />
                </div>
            </div>
            <div className="media-list">
                <DragAndDrop handleDrop={handleFile} >
                    <Row noGutters>
                        {
                            progress !== -1 && (
                                <Col md={3}>
                                    <div className="media-item m-2 shadow-sm" >
                                        <div className="preview" style={uploadFile.url ? { backgroundImage: `url(${uploadFile.url})` } : {}}>
                                            <Progress className="rounded-0" value={progress} className="position-absolute w-100" ></Progress>
                                        </div>
                                        <div className="p-2">
                                            <div className="media-name">{uploadFile.name}</div>
                                            <div className="small">{moment().format('DD MMM YYYY')}</div>
                                        </div>
                                    </div>
                                </Col>
                            )
                        }
                        {
                            list.map((item, inx) => {
                                let { path, name, attributes, updatedAt } = item
                                return (
                                    <Col md={3} key={inx}>
                                        <div className={`media-item m-2 shadow-sm ${allowSelect && selected && (selected.id === item.id) ? 'bg-primary text-white' : ''}`} onClick={() => allowSelect && onSelect(item)}>
                                            <div className="preview" style={{ backgroundImage: `url(${API_BASE}${path.replace(name, 'thumbnail-' + name)})` }}>
                                                <div className="action d-flex">
                                                    <a href={API_BASE + path} target="_blank" download={attributes.originalname} className="action-item bg-primary text-white ml-1">
                                                        <i className="simple-icon-cloud-download" />
                                                    </a>
                                                    <div className="action-item bg-danger text-white ml-1" onClick={() => deleteRow(item)}>
                                                        <i className="simple-icon-trash" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="p-2">
                                                <div className="media-name">{attributes.originalname}</div>
                                                <div className="small">{moment(updatedAt).format('DD MMM YYYY')}</div>
                                            </div>
                                        </div>
                                    </Col>
                                )
                            })
                        }
                    </Row>

                    {
                        list.length === 0 && (
                            <div className="p-5 text-center bg-white">
                                <h3>Dorp file here</h3>
                            </div>
                        )
                    }
                </DragAndDrop>
            </div>
            <Row className="my-3">
                <Col className="d-flex align-items-center">
                    <Input
                        type="select"
                        className="mr-2"
                        style={{ width: 100 }}
                        value={take}
                        onChange={({ target }) => setTake(target.value)}
                    >
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </Input>
                    <Label className="m-0">entries per page</Label>
                </Col>
                <Col>
                    <Pagination listClassName="d-flex justify-content-end">
                        <PaginationItem disabled={skip === 0}>
                            <PaginationLink previous onClick={() => setSkip(skip - 1)}></PaginationLink>
                        </PaginationItem>
                        {
                            paginationItems.map((item, inx) => {
                                if (
                                    item === 0 ||
                                    item === paginationItems.length - 1 ||
                                    item === skip ||
                                    item === skip - 1 ||
                                    item === skip + 1
                                ) {

                                    return (
                                        <PaginationItem active={skip === item} key={inx}>
                                            <PaginationLink onClick={() => setSkip(item)} >{item + 1}</PaginationLink>
                                        </PaginationItem>
                                    )
                                }
                                else {
                                    return ''
                                }
                            })
                        }
                        <PaginationItem disabled={paginationItems.length - 1 === skip}>
                            <PaginationLink next onClick={() => setSkip(skip + 1)}></PaginationLink>
                        </PaginationItem>
                    </Pagination>
                </Col>
            </Row>
        </React.Fragment>
    )
}


const mapStateToProps = ({ settings }) => {
    const { models } = settings;
    return { models };
};

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(MediaList)