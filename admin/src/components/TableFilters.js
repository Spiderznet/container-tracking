import React from 'react'
import ReactDatePicker from 'react-datepicker'
import { Button, FormGroup, Input, Label } from 'reactstrap'
import DatePicker from './Datepicker'
import SingleSelect from './SingleSelect'
import moment from 'moment'

// Define a default UI for filtering
export const DefaultColumnFilter = ({
    column: { filterValue, preFilteredRows, setFilter },
}) => {
    const count = preFilteredRows.length

    return (
        <Input
            value={filterValue || ''}
            onChange={e => {
                setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
            }}
            placeholder={`Search ${count} records...`}
        />
    )
}

// Define a default UI for filtering
export const DateRangeColumnFilter = ({
    column: { filterValue = [], preFilteredRows, setFilter, id },
}) => {
    const count = preFilteredRows.length

    const [startDate, endDate] = React.useMemo(() => {
        let dates = preFilteredRows.map(item => moment(item.values[id]))
        let startDate = moment.min(dates).startOf('day').valueOf()
        let endDate = moment.max(dates).endOf('day').valueOf()
        return [startDate, endDate]
    }, [id, preFilteredRows])
    // if (startDate == 0 && endDate == 0) return ""
    console.log(startDate, endDate, filterValue)
    return (
        <>
            <FormGroup>
                <Label>From</Label>
                <ReactDatePicker
                    className="w-100"
                    selected={filterValue[0] || startDate}
                    onChange={date => setFilter((old = []) => [moment(date).startOf('day').valueOf(), old[1] || endDate || null])}
                    selectsStart
                    startDate={filterValue[0] || startDate}
                    endDate={filterValue[1] || endDate}
                    dateFormat="dd MMM yyyy"
                />
            </FormGroup>
            <FormGroup>
                <Label>To</Label>
                <ReactDatePicker
                    className="w-100"
                    selected={filterValue[1] || endDate}
                    onChange={date => setFilter((old = []) => [old[0] || startDate || null, moment(date).endOf('day').valueOf()])}
                    selectsEnd
                    startDate={filterValue[0] || startDate}
                    endDate={filterValue[1] || endDate}
                    minDate={filterValue[0] || startDate}
                    dateFormat="dd MMM yyyy"
                />
            </FormGroup>
            <Button color="info" onClick={() => setFilter([startDate, endDate])}>Reset {id}</Button>
        </>
    )
    return (
        <DatePicker
            onChange={(name, value) => {
                const [start, end] = value;
                // console.log(value)
                // console.log(name, value)
                console.log('change', [start, end])
                setFilter([start, end])
            }}
            selected={filterValue[0] || startDate || new Date()}
            startDate={filterValue[0] || startDate || new Date()}
            endDate={filterValue[1] || endDate || new Date()}
            selectsRange
            inline
        />
    )
    return (
        <Input
            value={filterValue || ''}
            onChange={e => {
                setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
            }}
            placeholder={`Search ${count} records...`}
        />
    )
}

// This is a custom filter UI for selecting
// a unique option from a list
export const SelectColumnFilter = ({
    column: { filterValue, setFilter, preFilteredRows, id },
}) => {
    // Calculate the options for filtering
    // using the preFilteredRows
    // console.log({ preFilteredRows })
    const options = React.useMemo(() => {
        const options = new Set()
        preFilteredRows.forEach(row => {
            options.add(row.values[id])
        })
        return [...options.values()].map(i => ({ id: i, name: i }))
    }, [id, preFilteredRows])

    // Render a multi-select box

    return (
        <SingleSelect
            name={'filter'}
            onChange={(name, value) => {
                setFilter(value.id || undefined)
            }}
            options={[{ id: '', name: "All" }, ...options]}
        />
    )
    return (
        <Input type="select"
            value={filterValue}
            onChange={e => {
                setFilter(e.target.value || undefined)
            }}
        >
            <option value="">All</option>
            {options.map((option, i) => (
                <option key={i} value={option}>
                    {option}
                </option>
            ))}
        </Input>
    )
}

export const SliderColumnFilter = ({
    column: { filterValue, setFilter, preFilteredRows, id },
}) => {
    // Calculate the min and max
    // using the preFilteredRows

    const [min, max] = React.useMemo(() => {
        let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
        let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
        preFilteredRows.forEach(row => {
            min = Math.min(row.values[id], min)
            max = Math.max(row.values[id], max)
        })
        return [min, max]
    }, [id, preFilteredRows])
    return (
        <>
            <input
                type="range"
                min={min}
                max={max}
                value={filterValue || min}
                onChange={e => {
                    setFilter(parseInt(e.target.value, 10))
                }}
            />
            <button onClick={() => setFilter(undefined)}>Off</button>
        </>
    )
}

export const NumberRangeColumnFilter = ({
    column: { filterValue = [], preFilteredRows, setFilter, id },
}) => {
    const [min, max] = React.useMemo(() => {
        let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
        let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
        preFilteredRows.forEach(row => {
            min = Math.min(row.values[id], min)
            max = Math.max(row.values[id], max)
        })
        return [min, max]
    }, [id, preFilteredRows])

    return (
        <div
            className="d-flex align-items-center"
        >
            <Input
                value={filterValue[0] || ""}
                type="number"
                onChange={e => {
                    const val = e.target.value
                    setFilter((old = []) => [val || undefined, old[1] || max])
                }}
                placeholder={`Min (${min})`}
            />
        to
            <Input
                value={filterValue[1] || ""}
                type="number"
                onChange={e => {
                    const val = e.target.value
                    setFilter((old = []) => [old[0] || min, val || undefined])
                }}
                placeholder={`Max (${max})`}
            />
        </div>
    )
}