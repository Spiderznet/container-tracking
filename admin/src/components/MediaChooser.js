import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap'
import MediaList from './MediaList'

const MediaChooser = ({ models, name, onChange = () => { }, invalid = false, className = "", value, ...props }) => {
    let [open, setOpen] = useState(false)
    let [selected, setSelected] = useState(null)

    const toggle = () => setOpen(p => !p)

    const onClickFinish = () => {
        onChange(name, selected);
        toggle()
    }
    
    return (
        <React.Fragment>
            <div className={`${invalid ? 'is-invalid' : ''} ${className} ${name ? 'field-' + name : ''}`}>
                <div className="d-flex choose-box" onClick={toggle}>
                    <div className="px-3 py-2 bg-light"><b>Choose File</b></div>
                    <div className="px-3 py-2 border-left">{value && value.name}</div>
                </div>
            </div>
            <Modal size={'lg'} isOpen={open} toggle={toggle}>
                <ModalBody className="bg-light">
                    {open && (
                        <MediaList allowSelect={true} selected={selected} onSelect={selected => setSelected(selected)} />
                    )}
                </ModalBody>
                <ModalFooter className="py-3">
                    <Button color="primary" disabled={selected === -1} onClick={onClickFinish}>Finish</Button>
                </ModalFooter>
            </Modal>
        </React.Fragment >
    )
}

const mapStateToProps = ({ settings }) => {
    const { models } = settings;
    return { models };
};

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(MediaChooser)