import React from 'react'
import { Editor } from '@tinymce/tinymce-react'
import classNames from "classnames";
import { TINY_KEY } from '../config'


class RichEditor extends React.Component {

    handleChange = e => {
        let { onChange = () => { } } = this.props
        let value = e.target.getContent()
        onChange(value)
    }

    render() {
        let { handleChange } = this
        let { invalid = false, init = {} } = this.props
        return (
            <div className={classNames('rich-editor', { 'is-invalid': invalid })}>
                <Editor
                    {...this.props}
                    // apiKey={TINY_KEY}
                    init={{
                        menubar: false,
                        plugins: 'anchor link fullscreen lists autoresize blockquote indent image table',
                        toolbar: 'alignleft aligncenter alignright alignjustify | styleselect | link fullscreen numlist bullist | blockquote | indent | image | table',
                        // toolbar: 'bold italic underline | alignleft aligncenter alignright alignjustify | styleselect | link fullscreen numlist bullist',
                        content_css: false,
                        content_style: `
                            blockquote {
                                padding: 1rem 2rem;
                                border: solid 3px #ededed;
                                border-radius: 1rem;
                                display: inline-block;
                            }
                            blockquote:before {
                                content: '“';
                                font-size: 4rem;
                                position: absolute;
                                margin-left: -25px;
                                margin-top: -20px;
                            }
                            blockquote:after {
                                content: '”';
                                font-size: 4rem;
                                position: absolute;
                                margin-top: -20px;
                            }
                            blockquote * {
                                margin: 0;
                            }
                        `,
                        ...init
                    }}

                    onChange={handleChange}

                />
            </div>
        )
    }
}

export default RichEditor