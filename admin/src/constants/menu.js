import { adminRoot } from "./defaultValues";
import { UserRole } from "../helpers/authHelper"

const data = [
    {
        id: "dashboards",
        icon: "iconsminds-shop-4",
        label: "Dashboard",
        to: "/app/dashboard",
        roles: [UserRole.Admin, UserRole.root, UserRole.collector, UserRole.loaner]
    },
];
export default data;
