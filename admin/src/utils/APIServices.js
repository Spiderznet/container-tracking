import Axios from 'axios';
import { NotificationManager } from '../components/common/react-notifications';
import { API_ROOT } from '../config';
import data from '../data/notifications';

const getOptions = (options) => {
    let token = localStorage.getItem('token');
    if (token) {
        options = {
            ...options,
            headers: {
                'Authorization': `Bearer ${token}`
            }

        }
    }

    return options
}

const parseResponse = res => res.data;

const errorInfo = (res, err, handleError) => {
    if (handleError) return res
    if (!res.status) {
        NotificationManager.error(res.data, res.msg, 3000, null, null, '');
        // throw new Error(res.msg)
    }
    if (err) {
        NotificationManager.error(err.message, err.name, 3000, null, null, '');
        // throw err
    }
    return res
}

const http = {
    get: (url, config = {}, { handleError = false } = {}) => Axios.get(`${API_ROOT}${url}`, getOptions(config)).then(parseResponse).then((res, err) => errorInfo(res, err, handleError)),
    post: (url, data, config = {}, { handleError = false } = {}) => Axios.post(`${API_ROOT}${url}`, data, getOptions(config)).then(parseResponse).then((res, err) => errorInfo(res, err, handleError)),
    put: (url, data, config = {}, { handleError = false } = {}) => Axios.put(`${API_ROOT}${url}`, data, getOptions(config)).then(parseResponse).then((res, err) => errorInfo(res, err, handleError)),
    delete: (url, config = {}, { handleError = false } = {}) => Axios.delete(`${API_ROOT}${url}`, getOptions(config)).then(parseResponse).then((res, err) => errorInfo(res, err, handleError)),
}

export const AppServices = (path => ({
    getConfig: (data) => http.get(`${path}/app-config`, data)
}))('')

const AuthServices = {
    check: data => http.post('/auth/check', data),
    login: (username, password) => http.post('/auth/login', { username, password }),
    forgotPassword: data => http.post('/auth/forgot', data),
    verifyForgotPasswordOTP: data => http.post('/auth/forgot-verify-otp', data),
    resetPassword: data => http.post('/auth/forgot-reset', data),
    resendForgotPasswordOTP: data => http.post('/auth/forgot-resend-otp', data),
    changeChurch: data => http.post('/auth/change-church', data)
}

const UserServices = {
    list: (params = "") => http.get(`/users${params}`),
    addInfo: () => http.get(`/users/add-info`),
    insert: (data) => http.post(`/users`, data),
    details: (id) => http.get(`/users/${id}`),
    update: (id, data) => http.put(`/users/${id}`, data),
    delete: (id) => http.delete(`/users/${id}`)
}

const TerminalsServices = {
    list: (params = "") => http.get(`/terminals${params}`),
    addInfo: () => http.get(`/terminals/add-info`),
    insert: (data) => http.post(`/terminals`, data),
    details: (id) => http.get(`/terminals/${id}`),
    update: (id, data) => http.put(`/terminals/${id}`, data),
    delete: (id) => http.delete(`/terminals/${id}`)
}

const ShiftsServices = {
    list: (params = "") => http.get(`/shifts${params}`),
    addInfo: () => http.get(`/shifts/add-info`),
    insert: (data) => http.post(`/shifts`, data),
    details: (id) => http.get(`/shifts/${id}`),
    update: (id, data) => http.put(`/shifts/${id}`, data),
    delete: (id) => http.delete(`/shifts/${id}`)
}

const ContainersServices = {
    list: (params = "") => http.get(`/containers${params}`),
    addInfo: () => http.get(`/containers/add-info`),
    insert: (data) => http.post(`/containers`, data),
    details: (id) => http.get(`/containers/${id}`),
    update: (id, data) => http.put(`/containers/${id}`, data),
    delete: (id) => http.delete(`/containers/${id}`)
}

const EntriesServices = {
    list: (params = "") => http.get(`/entries${params}`),
    addInfo: () => http.get(`/entries/add-info`),
    insert: (data) => http.post(`/entries`, data),
    details: (id) => http.get(`/entries/${id}`),
    update: (id, data) => http.put(`/entries/${id}`, data),
    delete: (id) => http.delete(`/entries/${id}`)
}

const PaymentsServices = {
    list: (params = "") => http.get(`/admin/payments${params}`),
    addInfo: () => http.get(`/admin/payments/add-info`),
    insert: (data) => http.post(`/admin/payments`, data),
    details: (id) => http.get(`/admin/payments/${id}`),
    update: (id, data) => http.put(`/admin/payments/${id}`, data),
    delete: (id) => http.delete(`/admin/payments/${id}`)
}

const InstalmentsServices = {
    list: (params = "") => http.get(`/admin/instalments${params}`),
    addInfo: () => http.get(`/admin/instalments/add-info`),
    insert: (data) => http.post(`/admin/instalments`, data),
    details: (id) => http.get(`/admin/instalments/${id}`),
    update: (id, data) => http.put(`/admin/instalments/${id}`, data),
    delete: (id) => http.delete(`/admin/instalments/${id}`)
}

const ExpensesServices = {
    list: (params = "") => http.get(`/admin/expenses${params}`),
    addInfo: () => http.get(`/admin/expenses/add-info`),
    insert: (data) => http.post(`/admin/expenses`, data),
    details: (id) => http.get(`/admin/expenses/${id}`),
    update: (id, data) => http.put(`/admin/expenses/${id}`, data),
    delete: (id) => http.delete(`/admin/expenses/${id}`)
}

const LoanServices = {
    list: (params = "") => http.get(`/admin/loans${params}`),
    addInfo: () => http.get(`/admin/loans/add-info`),
    insert: (data) => http.post(`/admin/loans`, data),
    details: (id) => http.get(`/admin/loans/${id}`),
    update: (id, data) => http.put(`/admin/loans/${id}`, data),
    delete: (id) => http.delete(`/admin/loans/${id}`)
}

const LocationServices = {
    getCountry: (params = "") => http.get(`/locations/country${params}`),
    getState: (params = "") => http.get(`/locations/state${params}`),
    getCity: (params = "") => http.get(`/locations/city${params}`)
}
const DashboardServices = {
    getDashboardDetail: () => http.get('/admin/dashboard')
}



export {
    http,
    AuthServices,
    UserServices,
    LocationServices,
    DashboardServices,
    PaymentsServices,
    LoanServices,
    InstalmentsServices,
    ExpensesServices,
    TerminalsServices,
    ShiftsServices,
    ContainersServices,
    EntriesServices
}