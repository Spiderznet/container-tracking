const toFormData = (obj, form, namespace) => {
    let fd = form || new FormData();
    let formKey;

    for (let property in obj) {
        if (obj.hasOwnProperty(property) && obj[property]) {
            if (namespace) {
                formKey = namespace + '[' + property + ']';
            } else {
                formKey = property;
            }

            // if the property is an object, but not a File, use recursivity.
            if (obj[property] instanceof Date) {
                fd.append(formKey, obj[property].toISOString());
            }
            else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                toFormData(obj[property], fd, formKey);
            } else { // if it's a string or a File object
                fd.append(formKey, obj[property]);
            }
        }
    }

    return fd;
}

const formatAmount = (amount = 0) => {
    if ('toLocaleString' in window && typeof amount == "number") {
        return amount.toLocaleString('en-IN', {
            maximumFractionDigits: 2,
            style: 'currency',
            currency: 'INR'
        }).replace('.00', '').replace('₹', '₹ ');
    }
    return amount
}

const calculateTax = (taxList = [], amount = 0, addition = true) => {
    amount = taxList.reduce((total, item) => {
        if (addition) {
            total += item.is_percentage ? (amount * item.value) / 100 : item.value
        } else {
            total -= item.is_percentage ? (amount * item.value) / 100 : item.value
        }
        return total
    }, amount)
    return amount
}

export {
    toFormData,
    formatAmount,
    calculateTax
}