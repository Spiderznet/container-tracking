import React, { Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from '../layout/AppLayout';
import { ProtectedRoute, UserRole } from '../helpers/authHelper';


const BrowseView = React.lazy(() =>
    import(/* webpackChunkName: "viwes-browse-page" */ './Browse')
);
const DetailsView = React.lazy(() =>
    import(/* webpackChunkName: "viwes-details-page" */ './Details')
);
const CreateView = React.lazy(() =>
    import(/* webpackChunkName: "viwes-details-page" */ './Create')
);




const Models = ({ match, data = {}, ...props }) => {
    return (
        <Suspense fallback={<div className="loading" />}>
            <Switch>
                <Route
                    path={`${match.url}`}
                    render={routeProps => <BrowseView data={data} {...routeProps} />}
                    exact
                />
                <Route
                    path={`${match.url}/view/:id`}
                    render={routeProps => <DetailsView data={data} {...routeProps} />}
                    exact
                />
                <Route
                    path={`${match.url}/edit/:id`}
                    render={routeProps => <CreateView data={data} {...routeProps} />}
                    exact
                />
                <Route
                    path={`${match.url}/create`}
                    render={routeProps => <CreateView data={data} {...routeProps} />}
                    exact
                />

                <Redirect to="/error" />
            </Switch>
        </Suspense>
    );
};

const mapStateToProps = ({ menu }) => {
    const { containerClassnames } = menu;
    return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(Models));
