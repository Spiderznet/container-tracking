import React, { useEffect, useState } from 'react';
import { Row } from 'reactstrap';
import { sentenceCase } from 'sentence-case';
import { Colxx, Separator } from '../components/common/CustomBootstrap';
import ListGenerator from '../components/ListGenerator';
import { API_BASE } from '../config';
import Breadcrumb from '../containers/navs/Breadcrumb';
import { http } from '../utils/APIServices';
import moment from 'moment'
import { adminRoot } from '../constants/defaultValues';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const Details = ({ history, match, data = {}, models = [], ...props }) => {
    let { id } = match.params
    let { path, fields, title } = data

    let [details, setDetails] = useState({})
    const getDetails = () => {
        http.get(`${path}/${id}`).then(({ status, data }) => {
            if (status) {
                setDetails(data)
            }
        })
    }
    useEffect(() => {
        getDetails()
    }, [])
    let list = fields.reduce((final, field) => {
        let { browse = true, name, type } = field
        if (browse) {
            final.push({
                name,
                label: sentenceCase(name),
                Info: ({ value }) => {
                    if (['Int', 'String'].includes(type)) {
                        return value
                    }
                    if (type === 'DateTime') {
                        return moment(value).format('DD MMM YYYY')
                    }

                    if (type === 'Media') {
                        console.log(value)
                        return value !== null ? (
                            <React.Fragment>
                                <img src={API_BASE + value.path.replace(value.name, 'thumbnail-' + value.name)} width={300} alt={name} />
                                <a href={API_BASE + value.path} download target="_blank">
                                    <div className="mt-2"><i className="simple-icon-cloud-download" /> Download</div>
                                </a>
                            </React.Fragment>
                        ) : ''
                    }
                    let model = models.find(f => f.title === type)
                    if (model) {
                        return typeof value === 'object' && value !== null ? (
                            <a href={adminRoot + model.path + '/view/' + value.id}><i className="simple-icon-link" /> {value[browse.field]}</a>
                        ) : value || ''
                    }

                    return typeof value === 'object' && value !== null ? value[browse.field] : value || ''
                }
            })
        }
        return final
    }, []);

    return (
        <React.Fragment>
            <Row className="mb-4">
                <Colxx md={8}>
                    <Breadcrumb heading={`${title} Details`} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${adminRoot}${path}/edit/${id}`}><i className="simple-icon-note" /> Edit</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                    {
                        Object.keys(details).length && (

                            <ListGenerator
                                list={list}
                                values={details}
                            />
                        )
                    }
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

const mapStateToProps = ({ settings }) => {
    const { models } = settings;
    return { models };
};

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Details)