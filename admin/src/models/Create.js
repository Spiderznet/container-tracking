import { useFormik } from 'formik';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardTitle, Row } from 'reactstrap';
import { sentenceCase } from 'sentence-case';
import Swal from 'sweetalert2';
import { Colxx, Separator } from '../components/common/CustomBootstrap';
import FormGenerator from '../components/FormGenerator';
import { adminRoot } from '../constants/defaultValues';
import Breadcrumb from '../containers/navs/Breadcrumb';
import { http } from '../utils/APIServices';

const Create = ({ history, match, data = {}, models = [], enums = [], ...props }) => {
    let { id } = match.params
    let { path, fields = [], title } = data

    const formProps = useFormik({
        initialValues: {},
        initialValues: fields.reduce((final, field) => {
            let { create = true, name, type } = field
            if (create) {
                final[name] = undefined
            }
            return final
        }, {}),
        onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
            http[id ? 'put' : 'post'](id ? `${path}/${id}` : path, values, {}, { handleError: true }).then(({ status, msg, data }) => {
                setSubmitting(false)
                if (status) {
                    Swal.fire('Success', `${id ? 'Updated' : 'Created'} Successfuly`, 'success').then(() => {
                        history.push(`${adminRoot}${path}/view/${data.id}`)
                    })
                }
                else if (msg === "Validation error") {
                    setErrors(data)
                }
            })
        }
    })

    const formFields = fields.reduce((final, field) => {
        let { create = true, name, type, kind } = field
        let fieldType = '';
        let fieldProps = {};
        let Info = null;
        if (type === "String") {
            fieldType = 'text'
        }
        else if (type === "Int") {
            fieldType = 'number'
        }
        else if (type === "DateTime") {
            fieldType = 'date'
        }
        else if (type === "Media") {
            fieldType = 'Media'
        }
        else if (kind === 'enum') {
            let { values = [] } = enums.find(f => f.name === type) || {};

            console.log(values)
            fieldType = 'single-select'
            fieldProps = {
                options: values.map(({ name }) => ({ id: name, name }))
            }
        }
        else {
            let model = models.find(f => f.title === type)
            if (model) {
                fieldType = 'single-select-async'
                fieldProps = {
                    getOptionLabel: option => option[create.field || 'name'],
                    loadOptions: (search, callback) => {
                        let query = JSON.stringify({
                            _where: {
                                [create.field || 'name']: {
                                    contains: search
                                }
                            },
                        })
                        let config = {
                            params: {
                                qs: query
                            }
                        }
                        http.get(model.path, config).then(({ status, data }) => {
                            if (status) {
                                callback(data)
                            }
                        })
                    }
                }
            }
            else {
                fieldType = 'text'
            }
            Info = () => <a className="d-inline-block text-primary" target="_blank" href={`${adminRoot}${model.path}/create`}>Add New</a>
        }
        if (create) {
            let item = {
                name,
                label: sentenceCase(name),
                type: fieldType,
                props: fieldProps,
            }
            if (Info) {
                item['Info'] = Info
            }
            final.push(item)
        }
        return final
    }, [])

    useEffect(() => {
        if (id) {
            http.get(`${path}/${id}`).then(({ status, data }) => {
                if (status) {
                    console.log(status, data)
                    let values = fields.reduce((final, field) => {
                        let { create = true, name, type } = field
                        if (create) {
                            final[name] = data[name]
                        }
                        return final
                    }, {})
                    formProps.setValues(values)
                }
            })
        }
    }, [])

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8} >
                    <Breadcrumb heading={id ? "Edit " + title : "Create " + title} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    {id ? (
                        <Link className="btn btn-primary btn-lg" to={`${adminRoot}${path}/view/${id}`}><i className="simple-icon-eye" /> View Details</Link>
                    )
                        : ''}
                </Colxx>
            </Row>
            <Separator className="mb-5" />
            <Row className="mb-4">
                <Colxx xxs="12">
                    <Card body>
                        <CardTitle>
                            <i className="simple-icon-people mr-2"></i>{title} Detail Form
                        </CardTitle>
                        <FormGenerator
                            fields={formFields}
                            {...formProps}
                        />
                    </Card>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}
const mapStateToProps = ({ settings }) => {
    const { models, enums } = settings;
    return { models, enums };
};

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Create)