import React, { useEffect, useMemo, useState } from 'react';
import { http } from '../utils/APIServices';
import { sentenceCase } from 'change-case'
import { Badge, Button, ButtonGroup, Col, Input, Label, Pagination, PaginationItem, PaginationLink, Row } from 'reactstrap';
import { Colxx, Separator } from '../components/common/CustomBootstrap';
import Breadcrumb from '../containers/navs/Breadcrumb';
import { Link } from 'react-router-dom';
import ReactTable from '../components/ReactTable';
import moment from 'moment'
import { connect } from 'react-redux';
import { API_BASE } from '../config';
import { adminRoot } from '../constants/defaultValues';
import ExportTable from '../components/ExportTable';
import Swal from 'sweetalert2'

const Browse = ({ history, match, data = {}, models = [], ...props }) => {

    let [list, setList] = useState([])
    let [total, setTotal] = useState(0)
    let [paginationItems, setPaginationItems] = useState([])
    let [take, setTake] = useState('10')
    let [skip, setSkip] = useState(0)
    let [where, setWhere] = useState(0)
    let conditions = ['equals', 'not', 'in', 'notIn', 'lt', 'lte', 'gt', 'gte', 'contains', 'startsWith', 'endsWith']
    let [filters, setFilters] = useState([])
    let [openFilters, setOpenFilters] = useState(false)

    let { path, fields, title } = data

    const handleFilterChange = (inx, name, value) => {
        filters[inx][name] = value;
        setFilters([...filters])
    }

    const toggleFilter = () => {
        if (!openFilters && filters.length == 0) {
            addFilter()
        }
        setOpenFilters(!openFilters)
    }
    const addFilter = () => {
        let filter = {
            field: '',
            condition: conditions[0],
            value: ''
        }
        setFilters([...filters, filter])
    }
    const removeFilter = (inx) => {
        filters.splice(inx, 1);
        setFilters([...filters])
        if (filters.length == 0) {
            toggleFilter()
        }
    }

    const applyFilter = () => {
        filters = filters.filter(f => f.field)
        let whereCondition = filters.reduce((final, filter) => {
            final[filter.field] = {
                [filter.condition]: filter.value
            }
            return final
        }, {})
        setFilters(filters)
        setWhere(whereCondition)
        setOpenFilters(false)
    }

    const getList = () => {
        let query = JSON.stringify({
            _where: where,
            _sort: {
                createdAt: "desc"
            },
            _limit: Number(take),
            _offset: skip * Number(take)
        })
        let config = {
            params: {
                qs: query
            }
        }
        http.get(path, config).then(({ status, data }) => {
            if (status) {
                setList(data)
            }
        })

    }

    useEffect(() => {
        let pages = Math.ceil(total / Number(take))
        pages = Array(pages).fill().map((x, i) => i)
        setPaginationItems(pages)
    }, [take, total])

    useEffect(() => {
        getList()
    }, [take, skip, where])

    useEffect(() => {
        http.get(path + '/count').then(({ status, data }) => {
            if (status) {
                setTotal(data)
            }
        })
    }, [])

    const deleteRow = (row) => {
        Swal.fire({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this.",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(({ isConfirmed }) => {
            if (isConfirmed) {
                http.delete(`${path}/${row.id}`).then(({ status, data }) => {
                    if (status) {
                        getList()
                        Swal.fire('Success', 'Deleted Successfuly', 'success')
                    }
                })
            }
        })
    }

    const cols = useMemo(() => fields.reduce((final, field) => {
        let { browse = true, name, type } = field
        if (browse) {
            final.push({
                Header: sentenceCase(name),
                accessor: name,
                Cell: ({ value }) => {
                    if (['Int', 'String'].includes(type)) {
                        return value
                    }
                    if (type === 'DateTime') {
                        return moment(value).format('DD MMM YYYY')
                    }

                    if (type === 'Media') {
                        return value !== null ? (
                            <a href={API_BASE + value.path} download target="_blank">
                                <img src={API_BASE + value.path.replace(value.name, 'thumbnail-' + value.name)} width={100} alt={name} />
                            </a>
                        ) : ''
                    }
                    let model = models.find(f => f.title === type)
                    if (model) {
                        return typeof value === 'object' && value !== null ? (
                            <a href={adminRoot + model.path + '/view/' + value.id}>{value[browse.field]}</a>
                        ) : value || ''
                    }

                    return typeof value === 'object' && value !== null ? value[browse.field] : value || ''
                }
            })
        }
        return final
    }, []), [fields])

    let action = {
        Header: 'Action',
        accessor: 'action',
        cellClass: 'text-nowrap text-muted',
        Cell: ({ row }) => {
            return (
                <React.Fragment>
                    <Link to={`${adminRoot}${path}/view/${row.original.id}`} className="pr-2 pointer" >
                        <i className="simple-icon-eye" />
                    </Link>
                    <Link to={`${adminRoot}${path}/edit/${row.original.id}`} className="pr-2 pointer" >
                        <i className="simple-icon-note" />
                    </Link>
                    <span onClick={() => deleteRow(row.original)} className="pointer">
                        <i className="simple-icon-trash" />
                    </span>
                </React.Fragment>
            )
        }
    }

    return (
        <React.Fragment>
            <Row>
                <Colxx md={8}>
                    <Breadcrumb heading={title} match={match} />
                </Colxx>
                <Colxx md={4} className="text-right">
                    <Link className="btn btn-primary btn-lg" to={`${match.url}/create`}>Add New</Link>
                </Colxx>
                <Colxx xxs="12">
                    <Separator className="mb-5" />
                </Colxx>

                <Colxx xxs="12">
                    {
                        openFilters && (
                            <div>
                                {
                                    filters.map((filter, inx) => {
                                        return (
                                            <div className="d-flex mb-2" key={'filter-' + inx}>

                                                <Input className="mr-2" type="select" value={filter.field} onChange={({ target }) => handleFilterChange(inx, 'field', target.value)}>
                                                    <option value="">Select Field</option>
                                                    {
                                                        fields.filter(({ browse = true }) => browse).map((field, inx) => {
                                                            if (field.type !== 'String') return ''
                                                            return (
                                                                <option value={field.name} key={inx}>{sentenceCase(field.name)}</option>
                                                            )
                                                        })
                                                    }
                                                </Input>
                                                <Input className="mr-2" type="select" value={filter.condition} onChange={({ target }) => handleFilterChange(inx, 'condition', target.value)}>
                                                    {
                                                        conditions.map((condition, inx) => {
                                                            return (
                                                                <option value={condition} key={inx}>{sentenceCase(condition)}</option>
                                                            )
                                                        })
                                                    }
                                                </Input>
                                                <Input className="mr-2"
                                                    value={filter.value}
                                                    onChange={({ target }) => handleFilterChange(inx, 'value', target.value)}
                                                />
                                                <div className="d-flex" style={{ width: 100, flexShrink: 0 }}>
                                                    <div className="simple-icon-minus mr-2 pointer h3 m-0 text-primary" onClick={() => removeFilter(inx)}></div>

                                                    {
                                                        inx === filters.length - 1 && <div className="simple-icon-plus mr-2 pointer h3 m-0 text-primary" onClick={addFilter}></div>
                                                    }
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                                <div>
                                    <Button color="light" onClick={toggleFilter} className="ml-2" >Close</Button>
                                    <Button color="primary" onClick={applyFilter} className="ml-2" >Apply Filter</Button>
                                </div>
                            </div>
                        )
                    }
                    {
                        !openFilters && (
                            <div className="d-flex justify-content-between">
                                <div>
                                    <Button color="primary" onClick={toggleFilter} className="mr-2" ><i className="iconsminds-filter-2" />Filter</Button>
                                    {
                                        filters.map((item, inx) => {
                                            return (
                                                <Badge className="mr-1" color="light" pill key={inx}>{Object.values(item).join(' ')} <span className="pointer p-1 text-primary">&times;</span></Badge>
                                            )
                                        })
                                    }
                                </div>
                                {
                                    list.length !== 0 && (
                                        <ExportTable data={list} />
                                    )
                                }
                            </div>
                        )
                    }

                    <ReactTable columns={[...cols, action]} data={list} divided />
                    <Row className="my-3">
                        <Col className="d-flex align-items-center">
                            <Input
                                type="select"
                                className="mr-2"
                                style={{ width: 100 }}
                                value={take}
                                onChange={({ target }) => setTake(target.value)}
                            >
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </Input>
                            <Label className="m-0">entries per page</Label>
                        </Col>
                        <Col>
                            <Pagination listClassName="d-flex justify-content-end">
                                {/* <PaginationItem disabled={skip === 0}>
                                    <PaginationLink first onClick={() => setSkip(0)}></PaginationLink>
                                </PaginationItem> */}
                                <PaginationItem disabled={skip === 0}>
                                    <PaginationLink previous onClick={() => setSkip(skip - 1)}></PaginationLink>
                                </PaginationItem>
                                {
                                    paginationItems.map((item, inx) => {
                                        if (
                                            item === 0 ||
                                            item === paginationItems.length - 1 ||
                                            item === skip ||
                                            item === skip - 1 ||
                                            item === skip + 1
                                        ) {

                                            return (
                                                <PaginationItem active={skip === item} key={inx}>
                                                    <PaginationLink onClick={() => setSkip(item)} >{item + 1}</PaginationLink>
                                                </PaginationItem>
                                            )
                                        }
                                        else {
                                            return ''
                                        }
                                    })
                                }
                                <PaginationItem disabled={paginationItems.length - 1 === skip}>
                                    <PaginationLink next onClick={() => setSkip(skip + 1)}></PaginationLink>
                                </PaginationItem>
                                {/* <PaginationItem disabled={paginationItems.length - 1 === skip}>
                                    <PaginationLink last onClick={() => setSkip(paginationItems.length - 1)}></PaginationLink>
                                </PaginationItem> */}
                            </Pagination>
                        </Col>
                    </Row>
                </Colxx>
            </Row>
        </React.Fragment>
    )
}

const mapStateToProps = ({ settings }) => {
    const { models } = settings;
    return { models };
};

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Browse)